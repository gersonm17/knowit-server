'use strict';

/* Services */

var APIResources = function($resource)
{
	return {
		routes:
		{
			deviceSegment:appConfig.apiURL + '/devices/segment',
			survey:appConfig.apiURL + '/devices/survey',
			question:appConfig.apiURL + '/devices/question',
			customers:appConfig.apiURL + '/segments/customers',
            answer: appConfig.apiURL + '/devices/answer',
            //answerQuestion: appConfig.apiURL + '/devices/find',
            answerAverage: appConfig.apiURL  + '/devices/answer',
            enterprise:appConfig.apiURL  + '/companies/add',
            delete:appConfig.apiURL  + '/companies/company',
            device:appConfig.apiURL  + '/devices/device',
			devicePlace:appConfig.apiURL  + '/devices/save-place',
			deviceDelete:appConfig.apiURL  + '/devices/delete',
			place:appConfig.apiURL  + '/places/place',
			placeDelete:appConfig.apiURL  + '/places/delete'
            //excel:appConfig.apiURL  + '/report/excel'

		},
		users : $resource(appConfig.apiURL + '/users'),
		companies : $resource(appConfig.apiURL + '/companies'),
		devices : $resource(appConfig.apiURL + '/devices'),
		devicesad : $resource(appConfig.apiURL + '/devices/devicesadmin'),
		places : $resource(appConfig.apiURL + '/places'),
		segments : $resource(appConfig.apiURL + '/devices/segment'),
		surveys : $resource(appConfig.apiURL + '/devices/survey/:surveyId', {surveyId:'@id'}),
        excel:$resource(appConfig.apiURL  + '/report/excel')
        //answer: $resource(appConfig.apiURL + '/devices/answer/:segmentId',{segmentId:'@id'})
	};
};

var SystemFactory = function()
{
	return {
	};
};

KnowitApp.factory('APIResources', APIResources);
KnowitApp.factory('SystemFactory', SystemFactory);