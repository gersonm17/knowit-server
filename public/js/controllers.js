'use strict';

/* Controllers */
KnowitApp.controller('MenuController', ['$scope', '$location', function ($scope, $location) {

	$scope.isSmall = function () {
		return ($location.path() != '/');
	};

	$scope.isActive = function (path) {
		if ($location.path().substr(0, path.length) == path)
			return "active";
		else
			return "";
	};
}]);

KnowitApp.controller('PlacesModalController', ['$scope', '$modalInstance', '$http', 'APIResources', function ($scope, $modalInstance, $http, APIResources) {
	$scope.places = APIResources.places.get();
	$scope.devices=APIResources.devices.get();
	$scope.isPlaceOpen=false;
	$scope.isDeviceOpen=false;
	$scope.placeEditCopy={};
	$scope.deviceEditCopy={};
	$scope.currentPlace=null;
	$scope.deviceEdit=null;

	$scope.toggleNewPlace=function()
	{
		$scope.isPlaceOpen = !$scope.isPlaceOpen;
		$scope.placeEditCopy={};
		$scope.currentPlace={};
		$scope.placeEdit=null;
	};

	$scope.toggleNewDevice=function()
	{
		$scope.isDeviceOpen = !$scope.isPlaceOpen;
		$scope.deviceEditCopy.data={};
		$scope.deviceEdit=null;
		$scope.devices=APIResources.devices.get();
	};

	$scope.editDevice = function (_d) {
		for (var d in $scope.devices.data) {
			if ($scope.devices.data[d].id == _d.id) {
				$scope.devices.data[d].selected = true;
			} else $scope.devices.data[d].selected = 0;
		}
		$scope.deviceEditCopy.data = angular.copy(_d);
		$scope.deviceEdit = _d;
		$scope.isDeviceOpen = true;

	};

	$scope.togglePlace = function () {
		$scope.isPlaceOpen = !$scope.isPlaceOpen;
	};

	$scope.editPlace = function (_p) {
		$scope.placeEdit = _p;
		$scope.placeEditCopy = angular.copy(_p);
		$scope.isPlaceOpen=true;
		$scope.deviceEditCopy={};
	};

	$scope.selectPlace = function (_evt, _place) {
		$scope.currentPlace = _place;
		$(_evt.currentTarget).parent().parent().find('a').removeClass('active');
		$(_evt.currentTarget).addClass('active');
		$scope.deviceEditCopy={};
	};

	$scope.toggleDevice = function () {
		$scope.isDeviceOpen = !$scope.isDeviceOpen;
	};

	$scope.savePlace = function () {
		$http.put(APIResources.routes.place, $scope.placeEditCopy)
			.success(function (_data) {
				if ($scope.placeEdit)    angular.copy($scope.placeEditCopy, $scope.placeEdit);
				else {
					_data.data.devices = [];
					$scope.places.data.push(_data.data);
				}
				$scope.placeEditCopy = {};
				$scope.togglePlace();

			});
	};

	$scope.saveDevice = function () {
		$scope.deviceEditCopy.data[0]['place_id'] = $scope.currentPlace.id;
		$http.put(APIResources.routes.devicePlace, $scope.deviceEditCopy)
			.success(function (_data) {
				$scope.devices=APIResources.devices.get();
				if ($scope.deviceEdit) {
					angular.copy($scope.deviceEditCopy, $scope.deviceEdit);

				} else {
					$scope.currentPlace.devices.push(_data.data);
				}
				$scope.deviceEditCopy.data={};
				$scope.toggleDevice();
			});
	};

	$scope.deletePlace = function (_p) {
		if (confirm('Si eliminas este lugar se eliminarán también todas los dispositivos asociadas a esta')) {
			$http.post(APIResources.routes.placeDelete,{'id':_p.id})
				.success(function (_data) {
					if (_data.status.code == '200') {
						if (_p == $scope.deviceEditCopy) $scope.deviceEditCopy = null;
						$scope.places.data.splice($scope.places.data.indexOf(_p), 1);
					}
				}).error(function (_error) {
					console.log(_error);
				});
		}
	};


	$scope.deleteDevice = function (_p) {
		if (confirm('Si eliminas este dispositivo se eliminarán también toda la información asociadas a esta')) {
			$http.post(APIResources.routes.deviceDelete,{'id':_p.id})
				.success(function (_data) {
					if (_data.status.code == '200') {

						$scope.currentPlace.devices.splice($scope.currentPlace.devices.indexOf(_p), 1);
					}
				}).error(function (_error) {
					console.log(_error);
				});
		}
	};

	$scope.ok = function () {
		$modalInstance.close();
	};
	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
}]);

KnowitApp.controller('SurveyModalController', ['$scope', '$modalInstance', '$http', 'APIResources', function ($scope, $modalInstance, $http, APIResources) {
	$scope.surveys = APIResources.surveys.get();
	$scope.currentSurvey = null;
	$scope.isSurveyOpen = false;
	$scope.isQuestionOpen = false;

	$scope.surveyEdit = null;
	$scope.surveyEditCopy = {};
	$scope.questionEdit = null;
	$scope.questionEditCopy = {type: 'default'};

	$scope.selectSurvey = function (_evt, _survey) {
		$scope.currentSurvey = _survey;
		$(_evt.currentTarget).parent().parent().find('a').removeClass('active');
		$(_evt.currentTarget).addClass('active');
	};

	$scope.toggleSurvey = function () {
		$scope.isSurveyOpen = !$scope.isSurveyOpen;
	};
	$scope.toggleQuestion = function () {
		$scope.isQuestionOpen = !$scope.isQuestionOpen;
	};

	$scope.editSurvey = function (_s) {
		$scope.surveyEdit = _s;
		$scope.surveyEditCopy = angular.copy(_s);
		$scope.isSurveyOpen = true;
	};

	$scope.deleteSurvey = function (_s) {
		if (confirm('Si eliminas esta encuesta se eliminarán también todas las preguntas y respuestas asociadas a esta')) {
			$http.delete(APIResources.routes.survey + '/' + _s.id)
				.success(function (_data) {
					if (_data.status.code == '200') {
						if (_s == $scope.currentSurvey) $scope.currentSurvey = null;
						$scope.surveys.data.splice($scope.surveys.data.indexOf(_s), 1);
					}
				}).error(function (_error) {
					console.log(_error);
				});
		}
	};

	$scope.editQuestion = function (_q) {
		_q.has_comment = _q.has_comment.toString();
		$scope.questionEditCopy = angular.copy(_q);
		$scope.questionEdit = _q;
		$scope.isQuestionOpen = true;
	};

	$scope.deleteQuestion = function (_q) {
		if (confirm('Si eliminas esta pregunta se eliminarán también todas las respuestas asociadas a esta')) {
			$http.delete(APIResources.routes.question + '/' + _q.id)
				.success(function (_data) {
					if (_data.status.code == '200')
						$scope.currentSurvey.questions.splice($scope.currentSurvey.questions.indexOf(_q), 1);
				}).error(function (_error) {
					console.log(_error);
				});
		}
	};

	$scope.saveSurvey = function () {
		$http.put(APIResources.routes.survey, $scope.surveyEditCopy)
			.success(function (_data) {
				if ($scope.surveyEdit)    angular.copy($scope.surveyEditCopy, $scope.surveyEdit);
				else {
					_data.data.questions = [];
					$scope.surveys.data.push(_data.data);
				}
				$scope.surveyEditCopy = {};
				$scope.toggleSurvey();
			});
	};

	$scope.saveQuestion = function () {
		$scope.questionEditCopy.survey_id = $scope.currentSurvey.id;
		$http.put(APIResources.routes.question, $scope.questionEditCopy)
			.success(function (_data) {
				if ($scope.questionEdit) {
					angular.copy($scope.questionEditCopy, $scope.questionEdit);
					$scope.toggleQuestion();
				} else {
					$scope.currentSurvey.questions.push(_data.data);
				}
				$scope.questionEditCopy = {type: 'default'};
			});
	};

	$scope.ok = function () {
		$modalInstance.close();
	};
	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
}]);

KnowitApp.controller('ResumenController', function ($http, $scope, APIResources) {
	$scope.places = APIResources.places.get();
	$scope.segmentsSelected = [];
	$scope.timeout = null;
	$scope.isLoading = false;

	$scope.updateDetail = function () {
		$scope.segmentsSelected = [];
		for (var p in $scope.places.data) {
			var currentPlace = $scope.places.data[p];
			for (var d in currentPlace.devices) {
				var currentDevice = currentPlace.devices[d];
				for (var s in currentDevice.segments) {
					var currentSegment = currentDevice.segments[s];
					if (currentSegment.selected) {
						$scope.segmentsSelected.push({segment_id: currentSegment.id, device_id: currentDevice.id});
						currentPlace.hasSelected = true;
					}
				}
			}
		}

		clearTimeout($scope.timeout);
		if ($scope.segmentsSelected.length > 0) {
			$scope.timeout = setTimeout(function () {
				$scope.isLoading = true;
				$http.post(APIResources.routes.deviceSegment, $scope.segmentsSelected).success(function (_data) {
					$scope.isLoading = false;
					console.log('OK');
				}).error(function (_data) {
					$scope.isLoading = false;
					console.log('ERROR');
				});
			}, 800);
		}
	};

	$scope.selectAll = function (_place) {
		_place.hasSelected = true;
		for (var de in _place.devices) {
			for (var s in _place.devices[de].segments)
				_place.devices[de].segments[s].selected = true;
		}
		$scope.updateDetail();
	};

	$scope.selectNone = function (_place) {
		_place.hasSelected = false;
		for (var de in _place.devices) {
			for (var s in _place.devices[de].segments)
				_place.devices[de].segments[s].selected = false;
		}
		$scope.updateDetail();
	}
});

KnowitApp.controller('AdministradorController', ['$scope', '$route', '$routeParams', 'APIResources', '$http', 'SystemFactory', '$modal', function ($scope, $route, $routeParams, APIResources, $http, SystemFactory, $modal) {
	$scope.devices = APIResources.devices.get();
	$scope.surveys = APIResources.surveys.get();
	$scope.segments = APIResources.segments.get();

	$scope.currentDevice = null;
	$scope.currentSegment = null;
	$scope.currentSurvey = null;
	$scope.currentSegmentEditCopy = null;
	$scope.currentSegmentEdit = null;
	$scope.selectSurveyID = -1;
	$scope.selectDeviceID = -1;

	$scope.addSegmentOpen = false;

	$scope.selectDevice = function (_evt, _device) {
		$scope.currentDevice = _device;
		$scope.currentSurvey = null;
		$scope.currentSegment = null;
		$scope.currentSegmentEditCopy = null;
		$scope.currentSegmentEdit = null;

		$(_evt.currentTarget).parent().parent().find('a').removeClass('active');
		$(_evt.currentTarget).addClass('active');

		$routeParams.deviceID = _device.id;
	};

	$scope.removeFromDevice = function (_segment, _device) {
		//_device.segments();
		_device.segments.splice(_device.segments.indexOf(_segment), 1);

	};

	//Segments
	$scope.toggleAddSegment = function () {
		$scope.addSegmentOpen = !$scope.addSegmentOpen;
	};

	$scope.closeAddSegment = function () {
		$scope.addSegmentOpen = false;
		$scope.currentSegmentEditCopy = null;
		$scope.currentSegmentEdit = null;
	};

	$scope.saveSegment = function () {
		var deviceSelection = [];
		for (var d in $scope.currentSegmentEditCopy.devices)
			deviceSelection.push($scope.currentSegmentEditCopy.devices[d].id);

		$scope.currentSegmentEditCopy.devices_selection = deviceSelection;

		$http.put(APIResources.routes.deviceSegment, $scope.currentSegmentEditCopy)
			.success(function (_data) {
				if ($scope.currentSegmentEdit) {
					angular.copy($scope.currentSegmentEditCopy, $scope.currentSegmentEdit);
				} else {
					$scope.segments.data.push(_data.data);
				}

				$scope.devices = APIResources.devices.get();
				$scope.closeAddSegment();
			});
	};

	$scope.deleteSegment = function (_segment) {
		$http.delete(APIResources.routes.deviceSegment + '/' + _segment.id)
			.success(function (_data) {
				if (_data.status.code == '200')
					$scope.currentDevice.segments.splice($scope.currentDevice.segments.indexOf(_segment), 1);
			}).error(function (_error) {
				console.log(_error);
			});
	};
	$scope.editSegment = function (_segment) {
		for (var d in $scope.devices.data) {
			for (var ds in _segment.devices) {
				console.log(_segment.devices[ds].id + " " + $scope.devices.data[d].id + " > " + (_segment.devices[ds].id == $scope.devices.data[d].id));
				$scope.devices.data[d].selected = (_segment.devices[ds].id == $scope.devices.data[d].id);
				console.log("isticked: " + $scope.devices.data[d].ticked);
			}
		}

		console.log($scope.devices.data);

		$scope.currentSegmentEditCopy = angular.copy(_segment);
		$scope.currentSegmentEdit = _segment;
		$scope.addSegmentOpen = true;
	};

	$scope.selectSegment = function (_evt, _segment) {
		$scope.currentSegment = _segment;

		if ($scope.currentSegment.survey_id != null) {
			$scope.currentSurvey = APIResources.surveys.get({surveyId: $scope.currentSegment.survey_id});
			$scope.selectSurveyID = $scope.currentSegment.survey_id;
		} else {
			$scope.currentSurvey = null;
			$scope.selectSurveyID = -1;
		}

		$(_evt.currentTarget).parent().parent().find('a').removeClass('active');
		$(_evt.currentTarget).addClass('active');
	};

	//Surveys
	$scope.changeSurvey = function () {
		var params = {id: $scope.currentSegment.id, survey_id: $scope.selectSurveyID};

		$scope.currentSurvey = APIResources.surveys.get({surveyId: $scope.selectSurveyID});
		$scope.currentSegment.survey_id = $scope.selectSurveyID;
		$http.put(APIResources.routes.deviceSegment, params)
			.success(function (_data) {
				console.log(_data);
			});
	};

	$scope.editSurveys = function () {
		var modalInstance = $modal.open({
			templateUrl: appConfig.views + '/home/modal-surveys',
			controller: 'SurveyModalController'
		});

		modalInstance.result.then(function () {
			$scope.surveys = APIResources.surveys.get();
		}, function () {
			$scope.surveys = APIResources.surveys.get();
			if ($scope.currentSurvey != null) {
				$scope.currentSurvey = APIResources.surveys.get({surveyId: $scope.currentSegment.survey_id});
			}
		});
	}

	$scope.editPlace=function()
	{
		var modalInstance = $modal.open({
			templateUrl: appConfig.views + '/home/modal-places',
			controller: 'PlacesModalController'
		});

		modalInstance.result.then(function () {
			$scope.surveys = APIResources.places.get();
		}, function () {
			$scope.surveys = APIResources.places.get();
			if ($scope.currentSurvey != null) {
				$scope.currentSurvey = APIResources.surveys.get({surveyId: $scope.currentSegment.survey_id});
			}
		});
	}

}]);

KnowitApp.controller('ReportesController', ['$scope', 'APIResources', '$http', function ($scope, APIResources, $http)
{
	$scope.places = APIResources.places.get();
	$scope.mainChart = null;
	$scope.segmentsSelected = [];
	$scope.timeout = null;
	$scope.isLoading = false;
	$scope.customers = 0;
	$scope.start_r = $('#start_time');
	$scope.end_r = $('#end_time');

	$($scope.start_r).html(f_start);
	$($scope.start_r).datetimepicker({
		lang: 'es',
		timepicker: false,
		closeOnDateSelect: true,
		format: 'Y-m-d',
		onChangeDateTime: function (dp, $input) {
			$($scope.start_r).html($input.val()).val($input.val());
			$scope.updateDetail();
		}
	});
	$($scope.end_r).html(f_end);
	$($scope.end_r).datetimepicker({
		lang: 'es',
		timepicker: false,
		format: 'Y-m-d',
		closeOnDateSelect: true,
		onChangeDateTime: function (dp, $input) {
			$($scope.end_r).html($input.val()).val($input.val());
			$scope.updateDetail();
		}
	});

	$scope.getPdf = function () {
		console.log($scope.segmentsSelected);
	};

	$scope.updateDetail = function () {
		$scope.segmentsSelected = [];
		for (var p in $scope.places.data) {
			var currentPlace = $scope.places.data[p];
			for (var d in currentPlace.devices) {
				var currentDevice = currentPlace.devices[d];
				for (var s in currentDevice.segments) {
					var currentSegment = currentDevice.segments[s];
					if (currentSegment.selected) {
						$scope.segmentsSelected.push({
							place_id: currentPlace.id,
							segment_id: currentSegment.id,
							device_id: currentDevice.id,
							company_id: currentDevice.company_id
						});
						currentPlace.hasSelected = true;
						$('.btn-pdf').show();
					}//else $('.btn-pdf').hide();
				}
			}
		}
		selectedRange.segmentsSelected = $scope.segmentsSelected;
		clearTimeout($scope.timeout);
		if ($scope.segmentsSelected.length > 0) {
			$scope.timeout = setTimeout(function () {
				$scope.isLoading = true;
				$scope.paintReport();
				/*
				 $scope.paintChart();
				 $http.post(APIResources.routes.deviceSegment, $scope.segmentsSelected).success(function(_data){
				 $scope.isLoading = false;
				 console.log('OK');
				 }).error(function(_data){
				 $scope.isLoading = false;
				 console.log('ERROR');
				 });

				 /**/
			}, 800);
		} else $scope.customers = 0;
	};

	$scope.selectAll = function (_place) {
		_place.hasSelected = true;
		for (var de in _place.devices) {
			for (var s in _place.devices[de].segments)
				_place.devices[de].segments[s].selected = true;
		}
		$scope.updateDetail();
	};

	$scope.selectNone = function (_place) {
		_place.hasSelected = false;
		for (var de in _place.devices) {
			for (var s in _place.devices[de].segments)
				_place.devices[de].segments[s].selected = false;
		}

		$scope.updateDetail();
	};

//report//
	$scope.paintReport = function (s_id) {
		console.log($scope.segmentsSelected);
		$scope.chartsData = [];
		var segments_ = JSON.stringify($scope.segmentsSelected);
		var params = {segments: segments_, start_date: $('#start_time').val(), end_date: $('#end_time').val()};

		$http.post(APIResources.routes.answerAverage, params)
			.success(function (_data) {
				console.log('Carga de preguntas');
				console.log(_data);
				if (_data.status.code == "200") {
					console.log("OK");
					//-----------------------------------------------start chart -----------------------------------------------------------------
					var endDate = new Date();
					endDate.setHours(0);
					endDate.setMinutes(1);
					var ContentPromedio = $('#promedio_aprobacion');
					$(ContentPromedio).dxChart({
						//scale:{ startValue: startDate, endValue: endDate },
						valueAxis: {
							type: 'continuous',
							tick: {
								visible: true
							},
							constantLines: [{
								value: 10,
								color: '#6a5399'
							}],
							valueMarginsEnabled: true,
							minValueMargin: 0.5,
							maxValueMargin: 0.5,
							visible: true,
							strips: [
								{startValue: 19, endValue: 20, color: '#fff'},
								{startValue: 18, endValue: 19, color: '#C4F1E2'},
								{startValue: 17, endValue: 18, color: '#fff'},
								{startValue: 16, endValue: 17, color: '#C4F1E2'},
								{startValue: 15, endValue: 16, color: '#fff'},
								{startValue: 14, endValue: 15, color: '#C4F1E2'},
								{startValue: 13, endValue: 14, color: '#fff'},
								{startValue: 12, endValue: 13, color: '#C4F1E2'},
								{startValue: 11, endValue: 12, color: '#fff'},
								{startValue: 10, endValue: 11, color: '#C4F1E2'},
								{startValue: 9, endValue: 10, color: '#fff'},
								{startValue: 8, endValue: 9, color: '#C4F1E2'},
								{startValue: 7, endValue: 8, color: '#fff'},
								{startValue: 6, endValue: 7, color: '#C4F1E2'},
								{startValue: 5, endValue: 6, color: '#fff'},
								{startValue: 4, endValue: 5, color: '#C4F1E2'},
								{startValue: 3, endValue: 4, color: '#fff'},
								{startValue: 2, endValue: 3, color: '#C4F1E2'},
								{startValue: 1, endValue: 2, color: '#fff'},
								{startValue: 0, endValue: 1, color: '#C4F1E2'}
							]
							// strips:[{startValue: 10, endValue: 12, color: 'rgba(68, 100, 213, 0.2)' }]
						},
						argumentAxis: {
							tick: {
								visible: true
							}
						},
						tooltip: {
							enabled: true
						},
						legend: {
							visible: false
						},
						series: {
							type: 'line',
							argumentField: 'date',
							valueField: 'value',
							color: '#6a5399'
						}

					});
					var bool = [];
					for (var j in _data.boolQuestions) {
						bool[j] = _data.boolQuestions[j];
					}
					selectedRange.bools = bool;
					$scope.mainChart = $(ContentPromedio).dxChart("instance");
					if (_data.data.length > 0)$scope.chartsData = _data.data;

					$('.total-opiniones').text(_data.numberAnswer);//rpta recibi
					$('.peor-promedio').text(_data.customers);//toal d encustas
					$scope.customers = _data.customers;
					$('.mejor-promedio').text(_data.scoreKnowit + '%');
					$scope.mainChart.option('dataSource', $scope.chartsData);
					$scope.dat_circle = _data.questionCircle;
					$scope.paintChart();
				} else console.log("error");
			});

	};

	$scope.paintChart = function () {
		$('.face').show();
		$('.report-list-question').html('');
		var total_opiniones = 0;
		var idr = 0;
		var conttemp = 0;
		var countquestion = 0;
		for (var q in $scope.dat_circle) {
			countquestion++;
			if (conttemp <= 3) {
				conttemp++;
			} else {
				conttemp = 1;
				idr = 0;
			}
			if (idr == 0) {
				idr = $scope.dat_circle[q].id;
				var ulist = $('<ul style="height: 165px" class="list-questions list-tabs" id="ulsect' + idr + '" ></ul>');
				$('.report-list-question').append(ulist);
			}
			$('#ulsect' + idr).append('<li><div id="piechart-' + $scope.dat_circle[q].id + '" class="report-piechart"></div></li>');

			var dataSourcePie = new AdapterAnswerCount($scope.dat_circle[q].answers);
			$('#piechart-' + $scope.dat_circle[q].id).dxPieChart({
				animation: false,
				palette: dataSourcePie.getColors(),
				title: {text: $scope.dat_circle[q].question, font: {size: 13}},
				dataSource: dataSourcePie.getValues(),
				// dataSource:dataSource,
				series: {
					tooltip: {visible: true},
					legend: {visible: true}
					//argumentField: 'carita'
				}
			});
			total_opiniones += $scope.dat_circle[q].answers.length;
		}
		// $('.total-opiniones').html(total_opiniones);
		$('.total-preguntas').html(countquestion);
	};

//end//
}])
;

KnowitApp.controller('BaseDatosController', function ($http, $scope, APIResources) {
	$scope.places = APIResources.places.get();
	$scope.segmentsSelected = [];
	$scope.customers = null;
	$scope.timeout = null;
	$scope.isLoading = false;
	//
	var d_ = new Date('2015-02-01');
	$scope.dateStart = objHelp.convertDate(d_);
	$scope.dateEnd = objHelp.convertDate(new Date());
	$scope._since = "";
	$scope._until = "";
	$scope._start = $('#start_timebd');
	$($scope._start).html($scope.dateStart);
	$scope._since = $scope.dateStart;
	$scope._until = $scope.dateEnd;
	$($scope._start).datetimepicker({
		lang: 'es',
		timepicker: false,
		format: 'd-m-Y',
		onChangeDateTime: function (dp, $input) {
			$($scope._start).html($input.val()).val($input.val());
			$scope._since = $input.val();
			$scope.getCustomerByDate();
		}
	});
	$scope._end = $('#end_timebd');
	$($scope._end).html($scope.dateEnd);
	$($scope._end).datetimepicker({
		lang: 'es',
		timepicker: false,
		format: 'd-m-Y',
		onChangeDateTime: function (dp, $input) {
			$($scope._end).html($input.val()).val($input.val());
			$scope._until = $input.val();
			$scope.getCustomerByDate();
		}
	});
	$scope.getCustomerByDate = function () {
		$scope._page = 1;
		if ($scope._until !== "" && $scope._since !== "" && $scope.segmentsSelected.length > 0) {
			$scope.timeout = setTimeout(function () {
				$scope.isLoading = true;
				$http.post(APIResources.routes.customers, {
					selected: $scope.segmentsSelected,
					page: $scope._page,
					since: $scope._since,
					until: $scope._until
				}).success(function (_data) {
					$scope.isLoading = false;
					$scope.customers = _data;
					console.log('OK');
				}).error(function (_data) {
					$scope.isLoading = false;
					console.log('ERROR');
				});
			}, 500);
		}
	};
	/**/
	$scope.updateDetail = function (_page) {
		if (!_page) _page = 1;

		$scope.segmentsSelected = [];
		for (var p in $scope.places.data) {
			var currentPlace = $scope.places.data[p];
			for (var d in currentPlace.devices) {
				var currentDevice = currentPlace.devices[d];
				for (var s in currentDevice.segments) {
					var currentSegment = currentDevice.segments[s];
					if (currentSegment.selected) {
						$scope.segmentsSelected.push({segment_id: currentSegment.id, place_id: currentPlace.id});
						currentPlace.hasSelected = true;
					}
				}
			}
		}
		objHelp.since = $scope._since;
		objHelp.until = $scope._until;
		objHelp.segselec = $scope.segmentsSelected;
		clearTimeout($scope.timeout);
		if ($scope.segmentsSelected.length > 0) {
			$scope.timeout = setTimeout(function () {
				$scope.isLoading = true;
				var params = '';
				if ($scope._until !== "" && $scope._since !== "")params = {
					selected: $scope.segmentsSelected,
					page: _page,
					since: $scope._since,
					until: $scope._until
				};
				else params = {selected: $scope.segmentsSelected, page: _page};
				$http.post(APIResources.routes.customers, params).success(function (_data) {
					$scope.isLoading = false;
					$scope.customers = _data;
					console.log('OK');
				}).error(function (_data) {
					$scope.isLoading = false;
					console.log('ERROR');
				});
			}, 800);
		} else {
			$scope.customers = null;
		}
	};

	$scope.selectAll = function (_place) {
		_place.hasSelected = true;
		for (var de in _place.devices) {
			for (var s in _place.devices[de].segments)
				_place.devices[de].segments[s].selected = true;
		}
		$scope.updateDetail();
	};

	$scope.selectNone = function (_place) {
		_place.hasSelected = false;
		for (var de in _place.devices) {
			for (var s in _place.devices[de].segments)
				_place.devices[de].segments[s].selected = false;
		}
		$scope.updateDetail();
	};

});

KnowitApp.controller('KnowitController', ['$scope', '$route', '$routeParams', 'APIResources', '$http', 'SystemFactory', '$modal', function ($scope, $route, $routeParams, APIResources, $http, SystemFactory, $modal) {
	$scope.companies = APIResources.companies.get();
	$scope.devices = APIResources.devicesad.get();
	$scope.devicesActive=[{name: "Activo"},{name: "Inactivo"}];
	$scope.currentCompany = null;
	$scope.currentPlace = null;
	$scope.currentDevice = null;
	$scope.currentEnterpriseEditCopy = null;
	$scope.currentDeviceEditCopy = null;

	//device
	$scope.toggleAddPlace = function () {
		$scope.addDeviceOpen = !$scope.addDeviceOpen;
	};
	$scope.editDevice = function (_d) {
		for (var d in $scope.companies.data) {
			if ($scope.companies.data[d].id == _d.company_id) {
				$scope.companies.data[d].selected = true;
			} else $scope.companies.data[d].selected = 0;
		}

		$scope.currentDevice = _d;
		$scope.currentDeviceEditCopy = angular.copy(_d);
		$scope.addDeviceOpen = true;
	};
	$scope.saveDevice = function () {
		if($scope.currentDeviceEditCopy.company_id!=$scope.currentEnterpriseEditCopy[0].id)
		{
			if (confirm('Al cambiar de empresa, ser perdera toda la información de los segmentos ¿Desea continuar?'))
				$scope.currentDeviceEditCopy.company_id = $scope.currentEnterpriseEditCopy[0].id;
		}

		$http.put(APIResources.routes.device, $scope.currentDeviceEditCopy)
			.success(function (_data) {
				$scope.currentDeviceEditCopy = null;
				$scope.devices = APIResources.devicesad.get();
				$scope.closeAddDevice();
			});
	};
	/* $scope.deleteDevice= function(_d)
	 {
	 $scope.deviceDeleted = _d;
	 $http.delete(APIResources.routes.deviceSegment + '/' + _d.id)
	 .success(function(_data){
	 if(_data.status.code == '200')
	 $scope.currentDevice.segments.splice($scope.currentDevice.segments.indexOf(_segment), 1);
	 }).error(function(_error){
	 console.log(_error);
	 });
	 };/**/
	$scope.closeAddDevice = function () {
		$scope.addDeviceOpen = false;
		$scope.currentDeviceEditCopy = null;
		$scope.currentDeviceEdit = null;
	};

	//Enterprises
	$scope.toggleAddEnterprise = function () {
		$scope.addEnterpriseOpen = !$scope.addEnterpriseOpen;
		$scope.currentEnterpriseEditCopy = null;
	};
	$scope.closeAddEnterprise = function () {
		$scope.addEnterpriseOpen = false;
		$scope.currentEnterpriseEditCopy = null;
		$scope.currentEnterpriseEdit = null;
	};
	$scope.saveEnterprise = function () {
		//console.log($scope.currentEnterpriseEditCopy);
		if ($scope.currentEnterpriseEditCopy.name !== "") {
			if (typeof $scope.currentEnterpriseEditCopy.id == "undefined")var params = {
				name: $scope.currentEnterpriseEditCopy.name,
				description: $scope.currentEnterpriseEditCopy.description,
				user: $scope.currentEnterpriseEditCopy.user,
				password: $scope.currentEnterpriseEditCopy.password,
				email: $scope.currentEnterpriseEditCopy.email
			};
			else var params = {
				name: $scope.currentEnterpriseEditCopy.name,
				description: $scope.currentEnterpriseEditCopy.description,
				id: $scope.currentEnterpriseEditCopy.id,
				user: $scope.currentEnterpriseEditCopy.user,
				password: $scope.currentEnterpriseEditCopy.password,
				email: $scope.currentEnterpriseEditCopy.email
			}
			$http.put(APIResources.routes.enterprise, params)
				.success(function (_data) {
					$scope.currentEnterpriseEditCopy = null;
					$scope.companies = APIResources.companies.get();
					$scope.toggleAddEnterprise();
				});
		} else alert('Ingresar nombre');

		/**/
	};
	$scope.deleteEnterprise = function (_s) {
		$scope.deviceEnterprise = _s;
		$http.delete(APIResources.routes.delete + '/' + _s.id)
			.success(function (_data) {
				if (_data.status.code == '200')
					$scope.companies = APIResources.companies.get();
			}).error(function (_error) {
				console.log(_error);
			});
		/**/
	};
	$scope.editEnterprise = function (_s) {
		$scope.enterpriseEdit = _s;
		$scope.currentEnterpriseEditCopy = angular.copy(_s);
		$scope.addEnterpriseOpen = true;
	};
}]);