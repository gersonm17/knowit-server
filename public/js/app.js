'user strict'

/**
 * Geek Advice Angular App
 */
var KnowitApp = angular.module('KnowitApp', ['ngAnimate', 'ngRoute', 'ngResource', 'angular-loading-bar', 'mm.foundation', 'ui.tree', 'multi-select', 'pickadate']);

/*----------------------------------------------------------------------------------------------------- Routes */
KnowitApp.config(function($routeProvider, $tooltipProvider){
	$routeProvider
		.when('/', {
			templateUrl:appConfig.views + '/home/home'
		})
		.when('/resumen', {
			templateUrl:appConfig.views + '/home/resumen'
		})
		.when('/administrador/:deviceID?/:segmentID?', {
			templateUrl:appConfig.views + '/home/administrador'
		})
		.when('/reportes/:deviceID?/:segmentID?',{
			templateUrl:appConfig.views + '/home/reportes'
		})
		.when('/base-datos',{
			templateUrl:appConfig.views + '/home/base-datos'
		})
		.when('/knowit',{
			templateUrl:appConfig.views + '/home/knowit'
		})
		.otherwise({ redirectTo: '/' });

	$tooltipProvider.options({
		animation: false,
		appendToBody: false
	});

});

KnowitApp.animation(".icon-anim", function () {
	return {
		removeClass: function (element, className, done) {
			TweenMax.set(element, { opacity:1, width:'auto' });
			TweenMax.from(element,.5, { width:0, opacity:0, ease:Quart.easeOut, onComplete:done});
		},
		beforeAddClass: function (element, className, done) {
			TweenMax.to(element,.5, { width:0, opacity:0, ease:Quart.easeOut,  onComplete:done});
		}
	};
});

KnowitApp.animation(".hide-anim", function () {
	return {
		removeClass: function (element, className, done) {
			TweenMax.from(element,.5, { height:0, opacity:0, ease:Quart.easeOut, onComplete:done});
		},
		beforeAddClass: function (element, className, done) {
			var alto = $(element).css('height');
			TweenMax.to(element,.5, { height:0, opacity:0, ease:Quart.easeIn,  onComplete:function(){
				done();
				TweenMax.set(element, { opacity:1, height:alto });
			}});
		}
	};
});

KnowitApp.animation(".popup-anim", function () {
	return {
		removeClass: function (element, className, done) {
			TweenMax.from(element.find('.popup-overlay'),.3, {opacity:0});
			TweenMax.from(element.find('.popup-panel'),.3, {scale:0.5, opacity:0, ease:Quart.easeOut, onComplete:done});
		},
		beforeAddClass: function (element, className, done) {
			console.log(element);
			TweenMax.to(element,.5, {opacity:.6, onComplete:done});
		}
	};
});

KnowitApp.animation(".list-anim", function () {
	return {
		enter: function (element, done) {
			TweenMax.from(element,.5, {marginLeft:'-25px', opacity:0, ease:Quart.easeOut, onComplete:done});
		}
	};
});

KnowitApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) { cfpLoadingBarProvider.includeSpinner = false; }]);

/*----------------------------------------------------------------------------------------------------- Directivas */
KnowitApp.directive('appResizable', function($window) {
	return function($scope, $el, attr) {
		$scope.initializeWindowSize = function() {
			$scope.windowHeight = $window.innerHeight;
			var alto = $window.innerHeight - ($el[0].getBoundingClientRect().top + parseInt(attr.offsetBottom));
			if(alto < 0) alto = 0;
			$el.css({ height:alto+'px' });
			//$el.perfectScrollbar();
			return $scope.windowWidth = $window.innerWidth;
		};
		$scope.initializeWindowSize();
		return angular.element($window).bind('resize', function() {
			$scope.initializeWindowSize();
			//console.log('resize');
			return $scope.$apply();
		});
	};
});

KnowitApp.directive('appVersion', function() { return { template:'v0.98' }; });
KnowitApp.directive('status', function() {
	return {
		restrict:'E',
		template:'<span>Hola {{value}}</span>',
		link: function (scope, elem, attrs) {
			console.log(scope);
		},
		scope:
		{
			value : '='
		}
	};
});

function alertCtrl($scope) {
	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};
}

/**************************************************** Utils */


