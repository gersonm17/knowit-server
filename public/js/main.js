/*
 * jQuery UI Touch Punch 0.2.2
 *
 * Copyright 2011, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
(function(b){b.support.touch="ontouchend" in document;if(!b.support.touch){return;}var c=b.ui.mouse.prototype,e=c._mouseInit,a;function d(g,h){if(g.originalEvent.touches.length>1){return;}g.preventDefault();var i=g.originalEvent.changedTouches[0],f=document.createEvent("MouseEvents");f.initMouseEvent(h,true,true,window,1,i.screenX,i.screenY,i.clientX,i.clientY,false,false,false,false,0,null);g.target.dispatchEvent(f);}c._touchStart=function(g){var f=this;if(a||!f._mouseCapture(g.originalEvent.changedTouches[0])){return;}a=true;f._touchMoved=false;d(g,"mouseover");d(g,"mousemove");d(g,"mousedown");};c._touchMove=function(f){if(!a){return;}this._touchMoved=true;d(f,"mousemove");};c._touchEnd=function(f){if(!a){return;}d(f,"mouseup");d(f,"mouseout");if(!this._touchMoved){d(f,"click");}a=false;};c._mouseInit=function(){var f=this;f.element.bind("touchstart",b.proxy(f,"_touchStart")).bind("touchmove",b.proxy(f,"_touchMove")).bind("touchend",b.proxy(f,"_touchEnd"));e.call(f);};})(jQuery);

var selectedRange={};
/* Init Animations */
$(document).ready(initScript);

function initScript()
{
	ModalBox.init();
	MainNav.init();
}


/**
 * AlertBox Gestiona los mensajes y alertas dentro de la aplicación
 *
 * @type {{}}
 */
var AlertBox = {};
AlertBox.WARNING = 'alert-warning';
AlertBox.INFO = 'alert-info';
AlertBox.ERROR = 'alert-error';
/**
 * Mostrar alerta con un mensaje personalizado, se puede setear el tipo
 * @param _msg String con el mensaje a mostrar
 * @param _type String con el tipo de alerta a mostrar, ejemplo AlertBox.WARNING
 */
AlertBox.show = function(_msg, _type)
{
	var target = $('.block-alert');
	target.text(_msg);
	if(_type) target.addClass(_type);
	target.slideDown('fast');
};
/**
 * Oculta la alerta
 */
AlertBox.hide = function()
{
	$('.block-alert').slideUp('fast');
};
/**
 * Muestra una la alerta y la oculta luego de un tiempo determinado
 * @param _msg Mensaje a mostrar en la alerta
 * @param _type Tipo de alerta
 * @param _time Tiempo que se mostrará la alerta, por defecto son 3 seg
 */
AlertBox.flash = function(_msg, _type, _time)
{
	//TODO: Mostrar alerta por un tiempo determinado y ocultar
};


var MainNav = {};
MainNav.DOMObject = null;
MainNav.init = function()
{
	MainNav.DOMObject = $('#mainNav');
};

MainNav.goto = function(_url)
{
	var targetContent = $('#content');
	AlertBox.show('Cargando sección...');
	$.get(_url, function(_data){
		try {
			$('#content').html(_data);
		}catch (e) {
			console.error(e);
			alert('Error en la información cargada, por favor actualice la página');
		}
		//if(current.attr('href') != next.attr('href')) current.removeClass('active');
		targetContent.perfectScrollbar('update');
	}).always(function(){
			isLoading = false;
			AlertBox.hide();
		}).fail(function(){
			//next.removeClass('active');
			alert('Ocurrio un error');
		});
}
MainNav.reload = function()
{
	MainNav.DOMObject.find('a.active').click();
};


/**
 * Modal Box Gestiona los mensajes y alertas dentro de la aplicación
 *
 * @type {{}}
 */

var ModalBox = {};
ModalBox.DOMObject  = null;
ModalBox.options = null;
ModalBox.idle = false;
ModalBox.init = function()
{
	ModalBox.DOMObject = $('#modalBox');
	$('#overlay').click(function(){ ModalBox.close(); return false; });
	ModalBox.DOMObject.find('.modal-close').click(function(){ ModalBox.close(); return false; });
};

ModalBox.open = function(_content, _options)
{
	ModalBox.options = _options;
	if(_options)
	{
		if(_options.height){	ModalBox.DOMObject.height(_options.height); ModalBox.DOMObject.css('margin-top', -(_options.height/2)); }
		if(_options.width){		ModalBox.DOMObject.width(_options.width); ModalBox.DOMObject.css('margin-left', -(_options.width/2)); }
		if(_options.title)		ModalBox.DOMObject.find('.title').html(_options.title);
	}
	ModalBox.DOMObject.find('.modal-content').html(_content);
	ModalBox.DOMObject.find('.modal-content form').submit(function(){
		var form = $(this);
		var action = form.attr('action');
		var params = form.serializeArray();
		if(!ModalBox.idle)
		{
			ModalBox.idle = true;
			form.css('opacity', '0.5');
			$.post(action, params, function(){
				ModalBox.idle = false;
				form.css('opacity', '1');
				if(ModalBox.options.onSendComplete) ModalBox.options.onSendComplete();
			});
		}

		return false;
	});
	if(_options.onCreate) _options.onCreate(ModalBox.DOMObject);
	ModalBox.DOMObject.addClass('open');
	$('#overlay').fadeIn();
};

ModalBox.load = function()
{

};

ModalBox.close = function()
{
	if(ModalBox.options.onClose) ModalBox.options.onClose();

	ModalBox.DOMObject.removeClass('open');
	ModalBox.options = null;
	$('#overlay').fadeOut();
};

/*-------------------------------------------------------------------------------------------------------------------------- Charts */

var lineChartOption = {scaleFontSize:11, scaleFontColor:'#DDD', animation : false};
var DatasetChart = {};
DatasetChart.happy = function(_data)
{
	return {fillColor : "rgba(114,179,60,0.1)",
	strokeColor : "rgba(114,179,60,0.5)",
	pointColor : "rgba(114,179,60,0.5)",
	pointStrokeColor : "#fff",
	data : _data};
};
DatasetChart.smiley = function(_data)
{
	return {fillColor : "rgba(245,167,27,0.1)",
	strokeColor : "rgba(245,167,27,0.5)",
	pointColor : "rgba(245,167,27,0.5)",
	pointStrokeColor : "#fff",
	data : _data};
};
DatasetChart.neutral = function(_data)
{
	return {fillColor : "rgba(141,79,159,0.1)",
	strokeColor : "rgba(141,79,159,0.5)",
	pointColor : "rgba(141,79,159,0.5)",
	pointStrokeColor : "#fff",
	data : _data};
};
DatasetChart.sad = function(_data)
{
	return {fillColor : "rgba(58,109,181,0.1)",
	strokeColor : "rgba(58,109,181,0.5)",
	pointColor : "rgba(58,109,181,0.5)",
	pointStrokeColor : "#fff",
	data : _data};
};


function ChartModelDashboard(_data, _data2, _data3, _data4)
{
	this.labels = ["08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00"];
	this.datasets = [];

	if(_data) this.datasets.push(DatasetChart.sad(_data));
	if(_data2) this.datasets.push(DatasetChart.neutral(_data2));
	if(_data3) this.datasets.push(DatasetChart.smiley(_data3));
	if(_data4) this.datasets.push(DatasetChart.happy(_data4));
}

function ChartModelMoth(_data, _data2, _data3, _data4)
{
	var date = new Date(new Date().getFullYear(), new Date().getMonth(), 0);
	this.labels = [];
	for(var i=1; i <= date.getDate(); i++)
		this.labels.push(i);
	this.datasets = [];

	if(_data) this.datasets.push(DatasetChart.sad(_data));
	if(_data2) this.datasets.push(DatasetChart.neutral(_data2));
	if(_data3) this.datasets.push(DatasetChart.smiley(_data3));
	if(_data4) this.datasets.push(DatasetChart.happy(_data4));
};

Globalize.culture("es-ES");

/*--------------------------------------------------------------------------------------------------------------------------- Charts Adapters */
var myPalette = ['rgb(58,109,181)', 'rgb(141,79,159)', 'rgb(245,167,27)', 'rgb(114,179,60)'];
DevExpress.viz.core.registerPalette('emotionsColor', myPalette);

function AdapterAnswerCount(_data)
{
	var values;
	var values_;
	this.setValues = function(_answers){
		var result = [{arg:1, val:0}, {arg:2, val:0}, {arg:3, val:0}, {arg:4, val:0}];
		for(var _a in _answers)
		{
			//console.log(_answers[_a].raw_answer);
           if(!(selectedRange.bools.indexOf(_answers[_a].question_id)==-1)){

               switch (_answers[_a].raw_answer)
               {
                   case '0':
                       result[3].val++; break;
                   case '1':
                       result[0].val++; break;
               }
           }else{

               switch (_answers[_a].raw_answer)
               {
                   case '0':
                       result[0].val++; break;
                   case '1':
                       result[1].val++; break;
                   case '2':
                       result[2].val++; break;
                   case '3':
                       result[3].val++; break;
               }
           }

		}
		//values = result;
		//console.log(result);
		return result;
	};

	this.getColors = function()
	{
		//var myPalette = ['rgb(58,109,181)', 'rgb(141,79,159)', 'rgb(245,167,27)', 'rgb(114,179,60)'];
        //bool $color=array('#00A194','#71529D','','');
		//var myPalette = [  'rgb(114,179,60)','rgb(245,167,27)', 'rgb(141,79,159)','rgb(58,109,181)'];
		var myPalette = [  '#71529D','#8974A6', '#59BBB3','#00A194'];
		var result = [];
       // console.log(values);
		for(var i in values)
		{
			if(values[i].val > 0){
                result.push(myPalette[i]);
                //result.push(html_);
            }
		}

		return result;
	};

	this.getValues = function(){
        for(var i in values_)
        {
            //values_[i].arg=values_[i].val;//
            //values[i].arg=values[i].val;//
        }
		return values_;
	};

	//Constructor
	values = this.setValues(_data);
    values_ = values;
	//values = _data;
}