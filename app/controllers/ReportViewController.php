<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 17/09/14
 * Time: 04:54 PM
 */

class ReportViewController  extends BaseController {
    public $totalPages=0;
    public $nowPages=1;
    public $email='';

    public function getIndex()
    {
        $devices = Device::get();

        return View::make('reports', array(
            'devices' => $devices
        ));
    }

    public function getPdfDate()
    {
        $answersGroupByDeviceSeg = array ();
        $questionsGroup = array ();
        $countEncuesta = array ();
       /* $total_customer=0;
        $total_meta=0;*/
        $r = new ApiResponse();
        $segmentsSelected = json_decode (Input::get ('segments'));//array into necesary  and device is segment

        if (Auth::user ()->hasRole ('Admin')) {
            $flag = true;
        } else {
            $dataexist = DB::table ('customers')->where ('customers.company_id', '=', Auth::user ()->company->id)->get ();
            if (count ($dataexist) > 0) {
                $flag = true;
            }
           // $total_customer=count ($dataexist);
        }

        if ($flag) {
            if (is_array ($segmentsSelected)) {

                foreach ($segmentsSelected as $seg) {
                    $startDate = new DateTime(Input::get ('start_date'));
                    $endDate = new DateTime(Input::get ('end_date'));
                    $deviceId = $seg->device_id;
                   /* $segment=Segment::find($seg->segment_id);
                    $total_meta+=$segment->meta;*/
                    $answercusto = DB::table ('customers')->where ('customers.segment_id', '=', $seg->segment_id)/*->where('customers.place_id','=',$seg->place_id)*/
                    ->join ('answers', function ($join) use ($startDate, $endDate) {
                            $join->on ('customers.id', '=', 'answers.customer_id')->where ('answers.answer_at', '>=', $startDate->format ('Y-m-d H:i:s'))->where ('answers.answer_at', '<=', $endDate->format ('Y-m-d H:i:s'));
                        })->get ();

                    $tempAnswergroup[$seg->device_id] = $answercusto;/*->orderBy('answers.answer_at', 'asc')->get();*/

                    array_push ($answersGroupByDeviceSeg, $tempAnswergroup);
                    $tempEncuesta = DB::table ('customers')->where ('customers.segment_id', '=', $seg->segment_id)/*->where ('customers.place_id', '=', $seg->place_id)*/->where ('customers.company_id', '=', $seg->company_id)->where ('customers.register_at', '>=', $startDate->format ('Y-m-d H:i:s'))->where ('customers.register_at', '<=', $endDate->format ('Y-m-d H:i:s'))->count ();
                    array_push ($countEncuesta, $tempEncuesta);

                    $temp = DB::table ('device_segment')->where ('device_segment.device_id', '=', $deviceId)->where ('device_segment.segment_id', '=', $seg->segment_id)
                        //$device = DB::table('device_segment')->where('device_segment.device_id','=',$deviceId)->where('device_segment.segment_id','=',$seg->segment_id)
                        ->join ('segments', 'device_segment.segment_id', '=', 'segments.id')
                        ->join ('surveys', 'segments.survey_id', '=', 'surveys.id')
                        ->join ('questions',function($join)
                            {
                                $join->on('surveys.id', '=', 'questions.survey_id');
                                //->where('questions.type','=','default');
                            })->get ();
                    array_push ($questionsGroup, $temp);
                }
                
                $averageGroup = array ();
                $averageGroupSeg = array ();
                $rptabyQuestion = array ();
                $numberQuestions = array ();
                $numberQuestionsSeg = array ();
                
                foreach ($answersGroupByDeviceSeg as $seg => $obj) {
                    foreach ($obj as $d => $v) {
                        if(isset($v))
                        {
                            $startDate = new DateTime(Input::get ('start_date'));
                            $endDate = new DateTime(Input::get ('end_date'));
                            $data = KnowitMaths::getAnswerAveragePerDate ($v, $startDate, $endDate);

							if(isset($data))
                            {
								$averageGroup[$d] = $data;
                            }

                            $averageGroupSeg[$seg] = $averageGroup;
                            $numberQuestions[$d] = count ($v);
                            $numberQuestionsSeg[$seg] = $numberQuestions;
                            foreach ($v as $ans) {
                                $rptabyQuestion[] = $ans;
                            }

                        }

                    }

                }

                $questionsGroupFinal = array ();
                foreach ($questionsGroup as $sg) {
                    foreach ($sg as $q) {
                        $q->answers = array ();
                        $questionsGroupFinal[$q->id] = $q;
                    }
                }
                $count = 0;
                foreach ($rptabyQuestion as $clave => $valor) {
                    if (isset($questionsGroupFinal[$valor->question_id])) {
                        $questionsGroupFinal[$valor->question_id]->answers[] = $valor;
                    }
                    $count++;
                }
            }
        }

       // var_dump($custoTotal);
        //var_dump($answersGroupByDevice);
        //var_dump($averageGroup);
        //var_dump($questionsGroupFinal);
        //var_dump($devices);
        //var_dump($custoTotal);
        $pdf = new TCPDF();
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(false, 0);
        $pdf->Image('img/bg_report_big4.jpg', 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        //$pdf->setPageMark();

        $pdf->SetFont('creteround-regular');
        //$pdf->SetFont('helvetica');
        $pdf->SetTextColor(178,178,177);
        $pdf->SetFontSize(10);

        $pdf->Text(148,9,'Reporte generado el '  .date('d-m-Y'));
        $pdf->Text(181,13,'Hora: '.date('h:i'));

        $pdf->SetFontSize(14);
        //fechas
        $dateStart = new DateTime(Input::get('start_date'));
        $dateEnd = new DateTime(Input::get('end_date'));
        $pdf->Text(19.0,68.90,$dateStart->format('d/m/Y'));
        $pdf->Text(26.7,77.90,$dateEnd->format('d/m/Y'));
        $posDY = 69.2;
        /*foreach($devices as $seg_)
        {
            foreach($seg_ as $d)
            {
                $pdf->Text(85.43,$posDY,$d->name);
                $pdf->Text(155.43,$posDY,$d->lugar);
                $posDY += 6;
            }
        }*/

        $pdf->setJPEGQuality(90);
        $pdf->setImageScale(1.3);

        $largo = 0;
        //$min = 0;
        //$max = 0;


       // foreach($questionsGroup as $seg_){
            /*foreach($numberQuestions as $a){
                $largo += $a;
            }*/

        foreach ($numberQuestionsSeg as $numberQ) {
            foreach ($numberQ as $a) {
                $largo += $a;
            }
        }

       // }

        $sumT=0;
        $sumAverage=0;
        $div=0;//count($averageGroup);
        $averageBydate=array();

        foreach($averageGroupSeg as $g =>$v){
            $div += count ($v);
            foreach($v as $valv){
                foreach ($valv as $c => $j) {
                    //var_dump($j['date']);
                    if ($j['average'] == 0) $j['average'] = 10;
                    if (isset($averageBydate[$c])) {
                        $averageBydate[$c]['average'] = $averageBydate[$c]['average'] + floatval ($j['average']);
                        $averageBydate[$c]['date'] = $j['date'];
                    } else {
                        $averageBydate[$c]['average'] = array ();
                        $averageBydate[$c]['average'] = floatval ($j['average']);
                        $averageBydate[$c]['date'] = $j['date'];
                    }
                }
            }

            //var_dump(array_search('average', $v));
        }
        
        $averageFinal=0;
        $cc=0;

        foreach($averageBydate as $t=>$av){
            $avetemp=floatval($av['average']/$div);
            if(!($avetemp==10)){
                $averageFinal=$averageFinal+$avetemp;
                $cc++;
            }
            $averageBydate[$t]['date']=$av['date'];
            $averageBydate[$t]['average']=$avetemp;
        }
        

        $pdf->Image(KnowitGraphs::makeChart('', $averageBydate, 729, 376), 7, 106);

        $scoreKnowit=10;
        if($averageFinal!=0){
            $scoreKnowit= /*$averageFinal*100/20;*/$averageFinal/$cc;

        }
        $percentKnowit=$scoreKnowit*100/20;
     /*   $total_alcanze=0;
        if($total_customer!=0 && $total_meta>$total_customer)
        {
            $total_alcanze==(100*$total_customer)/$total_meta;
        }
        else if($total_customer>$total_meta)
        {
            $total_alcanze=0;
        }*/
        $pdf->SetTextColor(106,83,153);
        $pdf->SetFontSize(15);
        $pdf->Text(155,59.5,'Lugar');

        $pdf->SetTextColor(255,255,255);
        $pdf->SetFontSize(30);
        $pdf->Text(31-$pdf->GetStringWidth(count($questionsGroupFinal))/2,230,count($questionsGroupFinal));
        $encuestaTotal=0;
        foreach($countEncuesta as $v){
            $encuestaTotal+=$v;
        }
        $pdf->Text(80-$pdf->GetStringWidth($encuestaTotal)/2,230,$encuestaTotal);
        //var_dump($countrpta);
        $pdf->Text(125-$pdf->GetStringWidth(round($scoreKnowit, 2))/2,230,round($percentKnowit, 2).'%');
        $pdf->Text(177-$pdf->GetStringWidth(round($scoreKnowit, 2))/2,230,round($scoreKnowit, 2));

       // $pdf->SetFont('helvetica');

        $pdf->SetFontSize(10);
        $pdf->SetTextColor(113,82,157);

        $r_temp=array();//result temporais
        $r_temp2=array();//result temporais
        $cont_t=0;
        //var_dump($answersGroupByDevice);
      // var_dump($questionsGroupFinal);
       // foreach($answersGroupByDevice as $result){
            foreach($questionsGroupFinal as $q)
            {
                if($cont_t>=4){
                    $r_temp2[]=$r_temp;
                    $r_temp=array();
                    $cont_t=0;
                    $r_temp[]=$q;
                }else{
                    $r_temp[]=$q;
                }
                $cont_t++;
            }
       // }

        if(count($r_temp)>0)
        {
            $r_temp2[]=$r_temp;
        }
        $this->totalPages=count($r_temp2)+1;

        $pdf->Text(174,288,'Página 1 de '.$this->totalPages.'', false, false, true, 0,0,'r');
        $idlog = Auth::user()->id;
        $logUser = DB::table('users')->where('id','=',$idlog)->get();
        if(isset($logUser[0]->email))$this->email=$logUser[0]->email;
        $pdf->Text(78,282,''.$this->email.'', false, false, true, 0,0,'r');
        //-------------------------------------------------------------------------------------------- End Page 1
       //var_dump($r_temp2);
        foreach($r_temp2 as $groupQuestion){
             $this->nowPages++;
             $this->addNewPage($groupQuestion,$pdf);
        }

        $pdf->Output();
        $response = Response::make();
        $response->header('Content-Type', 'application/pdf');
        return $response;  /**/
    }

    public function addNewPage($result,$pdf){

        $pdf->AddPage();
        $pdf->SetAutoPageBreak(false, 0);
        $pdf->Image('img/bg_report_interna.jpg', 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

        $pdf->SetTextColor(178,178,177);
        $pdf->SetFontSize(10);

        $pdf->Text(148,9,'Reporte generado el '  .date('d-m-Y'));
        $pdf->Text(181,13,'Hora: '  .date('h:i'));
        $pdf->SetY(30);
        $pdf->SetX(8);

        $posY = 66;
        $posX = 10;//13
        $pfaX = 13 ; //intial postion faces X
        //$pfaY = 135; //intial postion faces y
        $pfaY = 129; //intial postion faces y

        $pfaXtemp=77;
        $pfaYtemp=80;
        //$posYy = 49;
        $posYy = 57;
        $posXx = 10;

        foreach($result as $q)
        {

            $co = KnowitMaths::getAnswerCount($q->answers);// 200 280
            //var_dump($co);
            if(empty($co)){
                $co=array(
                    't_0'=>0,
                    't_1'=>0,
                    't_2'=>0,
                    't_3'=>0
                );
            }else{
                //$pdf->Image('img/faces.jpg', $pfaX, $pfaY , 45,15 , '', '', '', false, '', '', false, false, 0);
               // var_dump($q);

            }

			if($q->type=='boolean'){
				//$pdf->Image('img/face_badgood.jpg', $pfaX, $pfaY , 40,7 , '', '', '', false, '', '', false, false, 0);
				$td=false;
			}else{
				//$pdf->Image('img/faces.jpg', $pfaX, $pfaY , 45,15 , '', '', '', false, '', '', false, false, 0);
				$td=true;

			}

            $longitud=39;
            $text=$q->question;
            $tempy=0;

            $pdf->Image(KnowitGraphs::makePie($q->type, array(isset($co['t_0'])?$co['t_0']:0, isset($co['t_1'])?$co['t_1']:0,isset($co['t_2'])?$co['t_2']:0, isset($co['t_3'])?$co['t_3']:0) , 235, 230), $posX, $posY);
            if($td){
                $pdf->Image('img/face.jpg', $pfaXtemp, $pfaYtemp , 20,36 , '', '', '', false, '', '', false, false, 0);
            }else {
                $pdf->Image('img/faceSino.jpg', $pfaXtemp, $pfaYtemp , 20,36 , '', '', '', false, '', '', false, false, 0);
            }

            if(strlen($text) <= $longitud){
                $pdf->Text($posXx,$posYy, ''.$text.'',false, false, true, 0,0,'');
            }else{
                while (strlen($text) > $longitud){
                    $texts=substr($text,0,$longitud);
                    $pos = strrpos($texts, " ");//ultima ap
                    if(strpos($texts, " ") !== false){
                        $texto=substr($texts,0,$pos);
                        $pdf->Text($posXx,$posYy+$tempy, ''.$texto.'',false, false, true, 0,0,'');
                        $text=str_replace($texto,"",$text);

                    }else{
                        $pdf->Text($posXx,$posYy+$tempy, ''.$texts.'',false, false, true, 0,0,'');
                        $text=str_replace($texts,"",$text);
                    }
                    $tempy=$tempy+4;
                    //$text=str_replace($texts,"",$texto);
                }
                if(strlen($text) <= $longitud){
                    $pdf->Text($posXx,$posYy+$tempy, ''.$text.'',false, false, true, 0,0,'');
                }
            }

            if($posX >= 110){
                $posY  += 105;
                $pfaY  += 105;
                $posYy += 105;

                $posX = 13;
                $pfaX = 13;
                $posXx = 13;

                $pfaXtemp =80;
                $pfaYtemp +=105;

            }else{
                $posX += 105;
                $pfaX += 120;
                $posXx += 120;

                $pfaXtemp +=102;

            }
            $pdf->SetX(8);

        }

        $pdf->SetTextColor(113,82,157);
        $pdf->Text(174,288,'Página '.$this->nowPages.' de '.$this->totalPages.'', false, false, true, 0,0,'r');
        $pdf->Text(78,282,''.$this->email.'', false, false, true, 0,0,'r');

    }

    public function getExcel()
	{
        $r = new ApiResponse();

        $customer = DB::table('customers');

        if(!Auth::user()->hasRole('Admin'))
            $customer->where('customers.company_id', '=', Auth::user()->company->id)->orderBy('register_at');

        //$_datad = Input::get('selected');


        if(Input::has('selected'))
        {
            //$_data = Input::get('selected');
            $_data = json_decode(Input::get('selected'));
            if(is_array($_data))
            {
                $customer->where(function($query) use ($_data){
                    foreach($_data as $d)
                    {
                        $query->orWhere(function($query2) use ($d){
                            $query2->where('segment_id', '=', $d->segment_id)/*->where('place_id', '=', $d['place_id'])*/;
                        });
                    }
                });
            }
        }

        if(Input::has('since') && Input::has('until'))
		{
            $since = new DateTime(Input::get('since'));
            $until = new DateTime(Input::get('until'));
            $until->setTime(23,59,59);
            $customer->where('customers.register_at', '>=', $since->format('Y-m-d H:i:s'))->where('customers.register_at', '<=', $until->format('Y-m-d H:i:s'));
        }

        $customer->join('segments as s', 'segment_id', '=', 's.id')->select(array('customers.*', 's.name as segment_name'))->orderBy('created_at', 'desc');

        $r->pagination['data'] = $customer->get();
        //$r->pagination = $customer->paginate(500)->toArray();
        //$customers = $r->pagination['data'];
        $customers = $r->pagination['data'];

        $r->pagination['data'] = null;
        foreach($customers as $c)
        {
            $answers = Answer::where('customer_id', '=', $c->id)->with('question')->get();
            $c->answers = $answers;
        }

        $datexcel=$customers;

        $data = array ();

        $result = array ();
        foreach ($datexcel as $valregister) {

            $segment = $valregister->segment_name;
            if (isset($result[$segment])) {
                $result[$segment][] = $valregister;
            } else {
                $result[$segment] = array ($valregister);
            }
        }
        unset($valregister);
        $count2 = 0;
        $count = 0;
        $tempArray=array();
        $val=null;
        $formdata_array=array();
        foreach ($result as $key => $valregister) {
            $count_person = 0;
            foreach ($valregister as $valperson) {

                /*if(($valperson->form_data)!='')
                {
                    $delete=array('{','}','"');
                    $formdata=str_replace($delete, '', $valperson->form_data);
                    $formdata=explode(',',$formdata);
                    $count=1;
                    //return Response::json($formdata);
                    foreach($formdata as $valformdata)
                    {
                        //return $valformdata;
                        $pos = strpos($valformdata, ':');
                        $pos++;
                        $val=trim(strval(substr($valformdata,$pos)));
                        $length=strlen($valformdata);
                        $length_val=strlen($val)+1;
                        $key2=trim(strval(substr($valformdata,-$length,-$length_val)));
                        $formdata_array[$count][$key2]=$val;
                        $count++;
                    }
                    //return $formdata_array;
                }*/

                $data[$key][$count_person]['Nombres'] = $valperson->name;
                $data[$key][$count_person]['E-mail'] = $valperson->email;
                $data[$key][$count_person]['Comentario'] = $valperson->comments;
                $data[$key][$count_person]['Fecha_registro'] = $valperson->register_at;
                $count2 = 0;
                if(count($valperson->answers)!=0)
                {
                    foreach ($valperson->answers as $valvalregister) {
                        if($valvalregister->question->type=='default')
                        {
                            $question = $valvalregister->question->question;
                            $data[$key][$count_person][$question] = $valvalregister->raw_answer + 1;
                        }

                        if ($valvalregister->question->type == 'custom') {
                            $question = $valvalregister->question->question;
                            $data[$key][$count_person][$question] = $valvalregister->comment;
                        }
                        if ($valvalregister->question->type == 'boolean') {
                            $question = $valvalregister->question->question;
                            $data[$key][$count_person][$question] = $valvalregister->question->options;
                        }

                        if ($valvalregister->question->type == 'options') {
                            $question = $valvalregister->question->question;
                            $data[$key][$count_person][$question] = $valvalregister->question->options;
                        }

                        //$data[$key][$count_person]["count"]=$count_person;
                        $count2++;
                    }
                    $count_person++;
                }
          }

            $count++;
        }



        Excel::create ('Reporte Knowit', function ($excel) use ($data) {
            foreach ($data as $key => $valdata) {
                if (count ($valdata) != 0) {
                    $maxVal=max($valdata);
                    $index0=$valdata[0];
                    //$indexN=$maxVal["count"];
                    //$valdata[0]=$valdata[$indexN];
                    //$valdata[$indexN]=$index0;
                    $cellCount= count($maxVal);
                    $cell = $cellCount + 1;
                } else {
                    $cell = 1;
                }


                switch ($cell) {
                    case 1:
                        $cell = ':A';
                        break;
                    case 2:
                        $cell = ':B';
                        break;
                    case 3:
                        $cell = ':C';
                        break;
                    case 4:
                        $cell = ':D';
                        break;
                    case 5:
                        $cell = ':E';
                        break;
                    case 6:
                        $cell = ':F';
                        break;
                    case 7:
                        $cell = ':G';
                        break;
                    case 8:
                        $cell = ':H';
                        break;
                    case 9:
                        $cell = ':I';
                        break;
                    case 10:
                        $cell = ':J';
                        break;
                    case 11:
                        $cell = ':K';
                        break;
                    case 12:
                        $cell = ':L';
                        break;
                    case 13:
                        $cell = ':M';
                        break;
                    case 14:
                        $cell = ':N';
                        break;
                    case 15:
                        $cell = ':O';
                        break;
                    case 16:
                        $cell = ':P';
                        break;
                    case 17:
                        $cell = ':Q';
                        break;
                    case 18:
                        $cell = ':R';
                        break;
                    case 19:
                        $cell = ':S';
                        break;
                    case 20:
                        $cell = ':T';
                        break;
                    case 21:
                        $cell = ':U';
                        break;
                    case 22:
                        $cell = ':V';
                        break;
                    case 23:
                        $cell = ':Q';
                        break;
                    case 24:
                        $cell = ':X';
                        break;
                    case 25:
                        $cell = ':Y';
                        break;
                    case 26:
                        $cell = ':Z';
                        break;
                }

                $excel->sheet ($key, function ($sheet) use ($valdata, $cell) {
                    $sheet->fromArray ($valdata, true, 'B' . '2', true, true);
                    if (count ($valdata) != 0) {
                        $num_bold = 2;
                        $num_border = count ($valdata) + 2;
                        $a = count ($valdata);

                        $sheet->cells ('B' . ($num_bold) . $cell . $num_bold, function ($cells) {
                            $cells->setBackground ('#5E2B8B');
                            $cells->setFontColor ('#ffffff');
                        });

                        $sheet->cells ('B' . ($num_bold + 1) . $cell . $num_border, function ($cells) {
                            $cells->setBackground ('#C4FFFF');
                        });

                        $sheet->setBorder ('B' . $num_bold . $cell . $num_border, 'thin', false, '#C1C1C1');
                        $sheet->setAutoSize (true);

                        $sheet->cells ('B' . $num_bold . $cell . $num_bold, function ($cells) {
                            $cells->setFont (array (
                                'family' => 'Calibri',
                                'size' => '11',
                                'bold' => true
                            ));
                        });
                    }
                });
            }
        })->export ('xls');
    }
} 