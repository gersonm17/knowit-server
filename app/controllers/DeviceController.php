<?php
/**
 * Created by PhpStorm.
 * User: Yuri
 * Date: 29/12/2014
 * Time: 13:44
 */

class DeviceController extends BaseController {

    protected $layout = 'ci.tpl_base';

    public function getIndex()
    {
        $devices = Device::get();

        $this->layout->content = View::make('admin.devices', array('devices'=>$devices));

    }
    public function getAddDevice($_deviceID)
    {
        $device = Device::find($_deviceID);

        if($device)
        {
            $device->disabled = 0;
            $device->company_id = Auth::user()->company->id;
            $device->save();
        }

        return Redirect::to('devices');
    }

} 