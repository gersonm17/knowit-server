<?php
/**
 * Created by PhpStorm.
 * User: Yuri
 * Date: 29/12/2014
 * Time: 13:44
 */

class SegmentController extends BaseController {
    public function getIndex()
    {
        $segments = Segment::get();
        //$devices = Device::get();

        return View::make('segmento', array(
            'segments'=> $segments
           // 'devices'=> $devices
        ));
    }
    public function postIndex()
    {
        return true;
    }

} 