<?php
/**
 * Created by PhpStorm.
 * User: Yuri
 * Date: 29/12/2014
 * Time: 15:25
 */

class QuestionController extends BaseController  {
    public function getIndex()
    {
        $segments = Segment::get();
        return View::make('pre', array(
            'segments'=> $segments
            // 'devices'=> $devices
        ));
    }

    public function getCompany($_id)
    {
        $r = new ApiResponse();
        $questions = Question::where('survey_id', '=', $_id)->get();
        if($questions)
        {
            $segment = DB::table('segments')->where('survey_id', '=', $_id)->get();
                return View::make('preguntas', array(
                'questions'=> $questions,
                'segment_id' => $segment[0]->id
            ));
            /**/
        }else{
            $dataEmpty=array();
            return View::make('preguntas', array(
                'questions'=>$dataEmpty
            ));
        }
    }

    public function postSave()
    {
        $r = new ApiResponse();

        Log::info('>> SaveAnsweringreso: ' . print_r($_REQUEST, true));

        $bad_answers = array();

        if(Request::get('answers'))
        {

        }else if(Request::get('code') && Request::get('answer') && Request::get('question_id'))
        {
            Log::info('>> else: ' .  print_r($_REQUEST, true));
            $data = new Answer();
            $data->segment_id = Request::get('segment_id');
            $data->question_id = Request::get('question_id');
            $data->raw_answer = Request::get('answer');
            $data->customer_id	 = 1;//default customer
            $data->answer_at	 = Request::get('answer_at');
            $data->created_at	 = date( "Y-m-d H:i:s");
            $data->save();

           // $r->setData($data->toArray());
            /**/
        }else{
            $r->status->setStatus(Status::STATUS_ERROR_PARAMETROS);
        }
        return json_encode($r->status->code);
    }
} 