<?php

class HomeController extends BaseController {

	protected $layout = 'ci.tpl_site';

	public function index()
	{
		$this->layout->content = View::make('home');
	}

	public function anyHome()
	{
		return View::make('home');
	}

	public function anyResumen()
	{
		return View::make('resumen');
	}

	public function anyAdministrador()
	{
		return View::make('administrador');
	}

	public function anyReportes()
	{
		return View::make('reportes');
	}

	public function anyBaseDatos()
	{
		return View::make('base_datos');
	}

	public function anyModalSurveys()
	{
		return View::make('modal_surveys');
	}

	public function anyModalPlaces()
	{
		return View::make('modal_place');
	}

	public function anyKnowit()
	{
		return View::make('knowit');
	}
}
