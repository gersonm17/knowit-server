<?php

class ApiSegmentController extends BaseController {

	public function getIndex()
	{
		$r = new ApiResponse();

		$devices = Auth::user()->company->devices->load('segments');

		$r->data = $devices->toArray();
		return Response::json($r);
	}

	public function anyGet()
	{
		$r = new ApiResponse();

		$companies = Device::get();

		$r->data = $companies->toArray();

		return Response::json($r);
	}

	public function postCustomers()
	{
		$r = new ApiResponse();

		$customer = DB::table('customers');

		if(!Auth::user()->hasRole('Admin'))
			$customer->where('customers.company_id', '=', Auth::user()->company->id);

		if(Input::has('selected'))
		{
			$_data = Input::get('selected');
			if(is_array($_data))
			{
				$customer->where(function($query) use ($_data){
					foreach($_data as $d)
					{
						$query->orWhere(function($query2) use ($d){
							$query2->where('segment_id', '=', $d['segment_id']);//->where('place_id', '=', $d['place_id'])
						});
					}
				});/**/
			}
		}
        if(Input::has('since') && Input::has('until')){
            $since = new DateTime(Input::get('since'));
            $until = new DateTime(Input::get('until'));
            $until->setTime(23, 59,59);
            $customer->where('customers.register_at', '>=', $since->format('Y-m-d H:i:s'))->where('customers.register_at', '<=', $until->format('Y-m-d H:i:s'));
        }
		$customer->join('segments as s', 'segment_id', '=', 's.id')->select(array('customers.*', 's.name as segment_name'))->orderBy('created_at', 'desc');

		$r->pagination = $customer->paginate(20)->toArray();
		$customers = $r->pagination['data'];
		$r->pagination['data'] = null;

		foreach($customers as $c)
		{
			$answers = Answer::where('customer_id', '=', $c->id)->with('question')->get();
			$c->answers = $answers;
		}

		$r->setData($customers);

		return Response::json($r);
	}
}
