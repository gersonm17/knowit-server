<?php

class ApiCompanyController extends BaseController {

	public function getIndex()
	{
		$r = new ApiResponse();

		$companies = Company::whereNull('deleted_at')->get();

		$r->data = $companies->toArray();

		return Response::json($r);
	}

	public function anyGet()
	{
		$r = new ApiResponse();

		$companies = Company::get();

		$r->data = $companies->toArray();

		return Response::json($r);
	}
    //Company
    public function putAdd()
    {
        $data=Input::all();
        $r = new ApiResponse();
        $error = false;

        if(Input::has('id')){
            $c = Company::find(Input::get('id'));
            if(Auth::user()->hasRole('Admin')){
                if(Input::has('name'))$c->name = Input::get('name');
                if(Input::has('description'))$c->description = Input::get('description');
                $c->save();
            }else $r->status->setStatus(Status::STATUS_ERROR_PARAMETROS);
        } else {
            $c = new Company();
            if(Auth::user()->hasRole('Admin'))$c->user_id = Auth::user()->id;
            if(Input::has('name'))$c->name = Input::get('name');
            if(Input::has('description'))$c->description = Input::get('description');
            $c->save();

            if(Input::has('user')&&Input::has('password')&&Input::has('email'))
            {
                $user = new User;
                $user->username=Input::get('user');
                $user->name=Input::get('user');
                $user->email = Input::get('email');
                $user->password=Input::get('password');
                $user->password_confirmation = Input::get('password');
                $user->confirmation_code = md5(uniqid(mt_rand(), true));
                $user->confirmed = 1;
                $user->company_id = $c->id;
                $user->save();
            }

            $r->data = $c->toArray();
            //$r->status->setStatus(Status::STATUS_ERROR_PARAMETROS);
        }
        return Response::json($r);
    }

    public function deleteCompany($_id){
        $r = new ApiResponse();
        $s = Company::find($_id);
        if($s){
            $s->deleted_at=date("Y-m-d h:i:s");
            $s->save();
            //->delete()
        }
        $r->data = $s->toArray();
        return Response::json($r);
    }
}
