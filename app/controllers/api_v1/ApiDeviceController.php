<?php

class ApiDeviceController extends BaseController
{

	public function getIndex()
	{
		$r = new ApiResponse();

		$devices = Auth::user()->company->devices->load('segments');
		//$devices = Device::all();
		$r->data = $devices;

		return Response::json($r);
	}

	public function putSavePlace()
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$data=Input::all();
		$r = new ApiResponse();
		$company_id=Auth::user()->company->id;
		if(isset($data['data'][0]['id']))
		{
			$place_id=$data['data'][0]['place_id'];
			$data = Device::find($data['data'][0]['id']);
			$data->place_id = $place_id;
			$data->save();
		}
		/*else if(Input::has('name') && Input::has('alias'))
		{
			$data = new Device();
			$data->company_id=$company_id;
			$data->uid=str_random(16);
			$data->name=Input::get('name');
			$data->alias=Input::get('alias');
			$data->disabled='1';
			if(Input::has('place_id')){$data->place_id = Input::get('place_id');}
			$data->save();
		}*/
		else{
			$r->status->setStatus(Status::STATUS_ERROR_PARAMETROS);
		}
		$r->data=$data->toArray();
		return json_encode($r);
	}

	public function getDevicesadmin()
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));
		$r = new ApiResponse();
		$devices = Device::all();
		$r->data = $devices;

		return Response::json($r);
	}

	//Devices
	public function putDevice()
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$data=Input::all();
		Log::info("INFO:data " . var_dump($data));
		$data=Input::all();
		//return $data;
		$r = new ApiResponse();
		$error = false;

		if (Input::has('id')) {
			$c = Device::find(Input::get('id'));
			$c->load('segments');
			//return Response::json($c);
			if (Auth::user()->hasRole('Admin') && $c !== null) {
				if (Input::has('name')) $c->name = Input::get('name');
				if (Input::has('company_id'))
				{
					if($c->company_id!=Input::get('company_id'))
					{
						if(count($c->segments)!=0)
						{
							foreach($c->segments as $segments)
							{
								$c->segments()->detach($segments->id);
							}
						}
						$c->company_id = Input::get('company_id');
						$divice=Device::where('uid',Input::get('uid'))->get();
						if(count($divice)!=0)
						{
							$divice[0]->company_id=Input::get('company_id');
							$divice[0]->disabled='0';
							$divice[0]->save();
							return $divice;
						}
					}
				}
				$c->save();
			} else $r->status->setStatus(Status::STATUS_ERROR_PARAMETROS);
		}
		return Response::json($r);
	}

	public function anyFind($_uid = null)
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));
		
		$r = new ApiResponse();

		Log::info('Busqueda de dispositivo ' . $_uid);

		if ($_uid != null) {
			$device = Device::where('uid', '=', $_uid)->with('segments.survey.questions')->first();

			if ($device) {
				if (!$device->disabled) {
					$r->data = $device->toArray();
				} else {
					$r->status->setStatus(Status::STATUS_ACCESS_DENIED);
					$r->status->details = 'Este dispositivo no está registrado en el sistema';
				}
			} else {
				Log::info('Dispositivo no encontrado ' . $_uid);
				$r->status->setStatus(Status::STATUS_ACCESS_DENIED);
				$r->status->details = 'Este dispositivo no está registrado en el sistema';

				if (count_chars($_uid) > 10) {
					$tempDevice = new Device();
					$tempDevice->uid = $_uid;
					$tempDevice->disabled = 1;
					$tempDevice->name = Input::get('model');
					if ($tempDevice->save())
						Log::info('Dispositivo guardado' . $_uid);
					else
						Log::info('No se pudo guardar el dispositivo ' . $_uid);
				}
			}
		} else {
			$r->status->setStatus(Status::STATUS_ERROR_PARAMETROS);
		}

		return Response::json($r);
	}

	public function postSync($_uid = null)
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$r = new ApiResponse();
		Log::info("INFO::::::: " . $_uid);

		try {
			if (Input::has('customers')) {
				foreach (Input::get('customers') as $c) {
					try {
						$customer = json_decode($c);
						$segment = Segment::find($customer->segment_id);
                        $device = Device::where('uid', $_uid)->first();
                        $duplicate = Customer::where('hash', $customer->hash)->first();
						if ($segment && $device && !$duplicate) {
							$entry = new Customer();
							$entry->name = $customer->name;
							$entry->last_name = $customer->last_name;
							$entry->email = $customer->email;
                            $entry->hash = $customer->hash;
                            $entry->created_at = date('Y-m-d h:i:s');
							if (isset($customer->form_data)) $entry->form_data = $customer->form_data . '}';
							if (isset($customer->phone)) $entry->phone = $customer->phone;
							if (isset($customer->company_name)) $entry->company_name = $customer->company_name;
							if (isset($customer->segment_id)) $entry->segment_id = $customer->segment_id;
							$entry->company_id = $segment->company->id;
							$entry->register_at = $customer->datetime;
							if (isset($customer->comments)) $entry->comments = $customer->comments;
							$entry->save();

							Log::info("Customer: " . print_r($entry->toArray(), true));
							foreach ($customer->answers as $a) {
								Log::info($a->value);
								$answer = new Answer();
								$answer->question_id = $a->question_id;
								$answer->raw_answer = $a->value;
								$answer->answer_at = $a->datetime;
								$answer->customer_id = $entry->id;
								$answer->segment_id = $customer->segment_id;
								if (isset($a->comment)) $answer->comment = $a->comment;
                                $answer->created_at = date('Y-m-d h:i:s');
                                $answer->save();
							}
							//$answer->value =
						}
					} catch (Exception $e) {
						Log::error("ERROR(postSaving:" . $c . ") :: " . $e->getMessage());
						$r->status->setStatus(Status::STATUS_ERROR_PROCESO);
						$r->status->details = "ERROR(postSaving:" . $c . ")";
					}
				}
			}
		} catch (Exception $e) {
			Log::error("ERROR(postSync) :: " . $e->getMessage());
			$r->status->setStatus(Status::STATUS_ERROR_PROCESO);
			$r->status->details = "ERROR(postSync)";
		}

		return Response::json($r);
	}
  /*  public function getSync2($_uid = null)
    {
        Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
        Log::info('INFO::PARAMETER',Input::all());
        Log::info('INFO::METHOD',array(Request::url()));

        $r = new ApiResponse();
        Log::info("INFO::::::: " . $_uid);

        try {
            $cs = '{
                "customers": [
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-01 10:12:58",
		  "question_id": 18,
		  "status": 0,
		  "customer": 499,
		  "id": 3612,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 10:13:03",
		  "question_id": 19,
		  "status": 0,
		  "customer": 499,
		  "id": 3613,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 10:13:07",
		  "question_id": 20,
		  "status": 0,
		  "customer": 499,
		  "id": 3614,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 10:13:11",
		  "question_id": 21,
		  "status": 0,
		  "customer": 499,
		  "id": 3615,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 10:13:16",
		  "question_id": 22,
		  "status": 0,
		  "customer": 499,
		  "id": 3616,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 10:13:19",
		  "question_id": 23,
		  "status": 0,
		  "customer": 499,
		  "id": 3617,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 10:13:22",
		  "question_id": 24,
		  "status": 0,
		  "customer": 499,
		  "id": 3618,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 10:13:25",
		  "question_id": 25,
		  "status": 0,
		  "customer": 499,
		  "id": 3619,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 10:13:31",
		  "question_id": 26,
		  "status": 0,
		  "customer": 499,
		  "id": 3620,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "muy agradecida con todos muy amorososn",
	  "datetime": "2016-09-01 10:12:41",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "Lizbel Breña ",
	  "segment_id": 2,
	  "status": 0,
	  "id": 499,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-01 11:23:59",
		  "question_id": 1,
		  "status": 0,
		  "customer": 500,
		  "id": 3621,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 11:24:01",
		  "question_id": 2,
		  "status": 0,
		  "customer": 500,
		  "id": 3622,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 11:24:04",
		  "question_id": 3,
		  "status": 0,
		  "customer": 500,
		  "id": 3623,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 11:24:06",
		  "question_id": 4,
		  "status": 0,
		  "customer": 500,
		  "id": 3624,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 11:24:08",
		  "question_id": 5,
		  "status": 0,
		  "customer": 500,
		  "id": 3625,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 11:24:09",
		  "question_id": 6,
		  "status": 0,
		  "customer": 500,
		  "id": 3626,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 11:24:11",
		  "question_id": 7,
		  "status": 0,
		  "customer": 500,
		  "id": 3627,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 11:24:13",
		  "question_id": 8,
		  "status": 0,
		  "customer": 500,
		  "id": 3628,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 11:24:15",
		  "question_id": 9,
		  "status": 0,
		  "customer": 500,
		  "id": 3629,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-01 11:23:57",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "valeria urbina",
	  "segment_id": 4,
	  "status": 0,
	  "id": 500,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-01 12:21:05",
		  "question_id": 27,
		  "status": 0,
		  "customer": 501,
		  "id": 3630,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 12:21:07",
		  "question_id": 28,
		  "status": 0,
		  "customer": 501,
		  "id": 3631,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 12:21:09",
		  "question_id": 29,
		  "status": 0,
		  "customer": 501,
		  "id": 3632,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 12:21:11",
		  "question_id": 30,
		  "status": 0,
		  "customer": 501,
		  "id": 3633,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 12:21:12",
		  "question_id": 31,
		  "status": 0,
		  "customer": 501,
		  "id": 3634,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 12:21:14",
		  "question_id": 32,
		  "status": 0,
		  "customer": 501,
		  "id": 3635,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 12:21:16",
		  "question_id": 33,
		  "status": 0,
		  "customer": 501,
		  "id": 3636,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-01 12:21:02",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "Carlos carrete florez",
	  "segment_id": 1,
	  "status": 0,
	  "id": 501,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-01 14:11:07",
		  "question_id": 10,
		  "status": 0,
		  "customer": 502,
		  "id": 3637,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 14:11:13",
		  "question_id": 11,
		  "status": 0,
		  "customer": 502,
		  "id": 3638,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 14:11:19",
		  "question_id": 12,
		  "status": 0,
		  "customer": 502,
		  "id": 3639,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 14:11:29",
		  "question_id": 13,
		  "status": 0,
		  "customer": 502,
		  "id": 3640,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 14:11:37",
		  "question_id": 14,
		  "status": 0,
		  "customer": 502,
		  "id": 3641,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 14:11:42",
		  "question_id": 15,
		  "status": 0,
		  "customer": 502,
		  "id": 3642,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 14:11:52",
		  "question_id": 16,
		  "status": 0,
		  "customer": 502,
		  "id": 3643,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-01 14:11:57",
		  "question_id": 17,
		  "status": 0,
		  "customer": 502,
		  "id": 3644,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-01 14:11:02",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "luz rivas ",
	  "segment_id": 3,
	  "status": 0,
	  "id": 502,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "2",
		  "datetime": "2016-09-02 10:13:20",
		  "question_id": 18,
		  "status": 0,
		  "customer": 503,
		  "id": 3645,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-02 10:13:25",
		  "question_id": 19,
		  "status": 0,
		  "customer": 503,
		  "id": 3646,
		  "tableName": "ANSWER"
		},
		{
            "value": "0",
		  "datetime": "2016-09-02 10:13:29",
		  "question_id": 20,
		  "status": 0,
		  "customer": 503,
		  "id": 3647,
		  "tableName": "ANSWER"
		},
		{
            "value": "0",
		  "datetime": "2016-09-02 10:13:45",
		  "question_id": 21,
		  "status": 0,
		  "customer": 503,
		  "id": 3648,
		  "tableName": "ANSWER"
		},
		{
            "value": "2",
		  "datetime": "2016-09-02 10:13:50",
		  "question_id": 22,
		  "status": 0,
		  "customer": 503,
		  "id": 3649,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-02 10:13:56",
		  "question_id": 23,
		  "status": 0,
		  "customer": 503,
		  "id": 3650,
		  "tableName": "ANSWER"
		},
		{
            "value": "2",
		  "datetime": "2016-09-02 10:14:01",
		  "question_id": 24,
		  "status": 0,
		  "customer": 503,
		  "id": 3651,
		  "tableName": "ANSWER"
		},
		{
            "value": "2",
		  "datetime": "2016-09-02 10:14:06",
		  "question_id": 25,
		  "status": 0,
		  "customer": 503,
		  "id": 3652,
		  "tableName": "ANSWER"
		},
		{
            "value": "2",
		  "datetime": "2016-09-02 10:14:13",
		  "question_id": 26,
		  "status": 0,
		  "customer": 503,
		  "id": 3653,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "estoy muy molesta con el servicio de las 3 técnicas fueron toscas e irrespetuosas . por el Dr. Pita que le tengo aprecio no he solicitado el libro de reclamaciones",
	  "datetime": "2016-09-02 10:12:35",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "Elizabeth Polo Gutierrez",
	  "segment_id": 2,
	  "status": 0,
	  "id": 503,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-02 12:08:35",
		  "question_id": 1,
		  "status": 0,
		  "customer": 504,
		  "id": 3654,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-02 12:08:36",
		  "question_id": 2,
		  "status": 0,
		  "customer": 504,
		  "id": 3655,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-02 12:08:38",
		  "question_id": 3,
		  "status": 0,
		  "customer": 504,
		  "id": 3656,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-02 12:08:41",
		  "question_id": 4,
		  "status": 0,
		  "customer": 504,
		  "id": 3657,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-02 12:08:43",
		  "question_id": 5,
		  "status": 0,
		  "customer": 504,
		  "id": 3658,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-02 12:08:45",
		  "question_id": 6,
		  "status": 0,
		  "customer": 504,
		  "id": 3659,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-02 12:08:47",
		  "question_id": 7,
		  "status": 0,
		  "customer": 504,
		  "id": 3660,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-02 12:08:49",
		  "question_id": 8,
		  "status": 0,
		  "customer": 504,
		  "id": 3661,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-02 12:08:50",
		  "question_id": 9,
		  "status": 0,
		  "customer": 504,
		  "id": 3662,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-02 12:08:33",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "Rosa Espinoza",
	  "segment_id": 4,
	  "status": 0,
	  "id": 504,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-03 09:51:41",
		  "question_id": 10,
		  "status": 0,
		  "customer": 505,
		  "id": 3663,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-03 09:51:43",
		  "question_id": 11,
		  "status": 0,
		  "customer": 505,
		  "id": 3664,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-03 09:51:45",
		  "question_id": 12,
		  "status": 0,
		  "customer": 505,
		  "id": 3665,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-03 09:51:46",
		  "question_id": 13,
		  "status": 0,
		  "customer": 505,
		  "id": 3666,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-03 09:51:48",
		  "question_id": 14,
		  "status": 0,
		  "customer": 505,
		  "id": 3667,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-03 09:51:50",
		  "question_id": 15,
		  "status": 0,
		  "customer": 505,
		  "id": 3668,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-03 09:51:59",
		  "question_id": 16,
		  "status": 0,
		  "customer": 505,
		  "id": 3669,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-03 09:52:00",
		  "question_id": 17,
		  "status": 0,
		  "customer": 505,
		  "id": 3670,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-03 09:51:39",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "marilyn  canales",
	  "segment_id": 3,
	  "status": 0,
	  "id": 505,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-05 12:38:52",
		  "question_id": 27,
		  "status": 0,
		  "customer": 506,
		  "id": 3671,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 12:38:53",
		  "question_id": 28,
		  "status": 0,
		  "customer": 506,
		  "id": 3672,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 12:38:56",
		  "question_id": 29,
		  "status": 0,
		  "customer": 506,
		  "id": 3673,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 12:38:57",
		  "question_id": 30,
		  "status": 0,
		  "customer": 506,
		  "id": 3674,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-05 12:38:50",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "",
	  "segment_id": 1,
	  "status": 0,
	  "id": 506,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [],
	  "comments": "",
	  "datetime": "2016-09-05 12:38:59",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "",
	  "segment_id": 1,
	  "status": 0,
	  "id": 507,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-05 12:39:03",
		  "question_id": 31,
		  "status": 0,
		  "customer": 508,
		  "id": 3675,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 12:39:05",
		  "question_id": 32,
		  "status": 0,
		  "customer": 508,
		  "id": 3676,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 12:39:07",
		  "question_id": 33,
		  "status": 0,
		  "customer": 508,
		  "id": 3677,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-05 12:39:02",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "maria Fernanda zavala",
	  "segment_id": 1,
	  "status": 0,
	  "id": 508,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [],
	  "comments": "",
	  "datetime": "2016-09-05 12:39:25",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "",
	  "segment_id": 1,
	  "status": 0,
	  "id": 509,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [],
	  "comments": "",
	  "datetime": "2016-09-05 12:39:27",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "",
	  "segment_id": 1,
	  "status": 0,
	  "id": 510,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-05 15:26:00",
		  "question_id": 10,
		  "status": 0,
		  "customer": 511,
		  "id": 3678,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:26:02",
		  "question_id": 11,
		  "status": 0,
		  "customer": 511,
		  "id": 3679,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:26:03",
		  "question_id": 12,
		  "status": 0,
		  "customer": 511,
		  "id": 3680,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:26:05",
		  "question_id": 13,
		  "status": 0,
		  "customer": 511,
		  "id": 3681,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:26:09",
		  "question_id": 14,
		  "status": 0,
		  "customer": 511,
		  "id": 3682,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:26:11",
		  "question_id": 15,
		  "status": 0,
		  "customer": 511,
		  "id": 3683,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:26:12",
		  "question_id": 16,
		  "status": 0,
		  "customer": 511,
		  "id": 3684,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:26:14",
		  "question_id": 17,
		  "status": 0,
		  "customer": 511,
		  "id": 3685,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-05 15:25:58",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "irma urteaga",
	  "segment_id": 3,
	  "status": 0,
	  "id": 511,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-05 15:27:39",
		  "question_id": 10,
		  "status": 0,
		  "customer": 512,
		  "id": 3686,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:27:46",
		  "question_id": 11,
		  "status": 0,
		  "customer": 512,
		  "id": 3687,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:27:50",
		  "question_id": 12,
		  "status": 0,
		  "customer": 512,
		  "id": 3688,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:27:55",
		  "question_id": 13,
		  "status": 0,
		  "customer": 512,
		  "id": 3689,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:28:12",
		  "question_id": 14,
		  "status": 0,
		  "customer": 512,
		  "id": 3690,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:28:19",
		  "question_id": 15,
		  "status": 0,
		  "customer": 512,
		  "id": 3691,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:28:28",
		  "question_id": 16,
		  "status": 0,
		  "customer": 512,
		  "id": 3692,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 15:28:34",
		  "question_id": 17,
		  "status": 0,
		  "customer": 512,
		  "id": 3693,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-05 15:27:22",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "",
	  "segment_id": 3,
	  "status": 0,
	  "id": 512,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "2",
		  "datetime": "2016-09-05 16:02:31",
		  "question_id": 10,
		  "status": 0,
		  "customer": 513,
		  "id": 3694,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:02:52",
		  "question_id": 11,
		  "status": 0,
		  "customer": 513,
		  "id": 3695,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:02:58",
		  "question_id": 12,
		  "status": 0,
		  "customer": 513,
		  "id": 3696,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:03:02",
		  "question_id": 13,
		  "status": 0,
		  "customer": 513,
		  "id": 3697,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:03:08",
		  "question_id": 14,
		  "status": 0,
		  "customer": 513,
		  "id": 3698,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:03:13",
		  "question_id": 15,
		  "status": 0,
		  "customer": 513,
		  "id": 3699,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:03:19",
		  "question_id": 16,
		  "status": 0,
		  "customer": 513,
		  "id": 3700,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:03:23",
		  "question_id": 17,
		  "status": 0,
		  "customer": 513,
		  "id": 3701,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-05 16:02:23",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "Lizbelle Breña ",
	  "segment_id": 3,
	  "status": 0,
	  "id": 513,
	  "tableName": "CUSTOMER"
	},
	{
        "answers": [
		{
            "value": "3",
		  "datetime": "2016-09-05 16:03:54",
		  "question_id": 10,
		  "status": 0,
		  "customer": 514,
		  "id": 3702,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:03:59",
		  "question_id": 11,
		  "status": 0,
		  "customer": 514,
		  "id": 3703,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:04:05",
		  "question_id": 12,
		  "status": 0,
		  "customer": 514,
		  "id": 3704,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:04:10",
		  "question_id": 13,
		  "status": 0,
		  "customer": 514,
		  "id": 3705,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:04:19",
		  "question_id": 14,
		  "status": 0,
		  "customer": 514,
		  "id": 3706,
		  "tableName": "ANSWER"
		},
		{
            "value": "2",
		  "datetime": "2016-09-05 16:04:24",
		  "question_id": 15,
		  "status": 0,
		  "customer": 514,
		  "id": 3707,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:04:28",
		  "question_id": 16,
		  "status": 0,
		  "customer": 514,
		  "id": 3708,
		  "tableName": "ANSWER"
		},
		{
            "value": "3",
		  "datetime": "2016-09-05 16:04:32",
		  "question_id": 17,
		  "status": 0,
		  "customer": 514,
		  "id": 3709,
		  "tableName": "ANSWER"
		}
	  ],
	  "comments": "",
	  "datetime": "2016-09-05 16:03:45",
	  "dni": "",
	  "email": "",
	  "last_name": "",
	  "name": "kiomy Rodriguez",
	  "segment_id": 3,
	  "status": 0,
	  "id": 514,
	  "tableName": "CUSTOMER"
	}
  ]
}';
            foreach(json_decode($cs) as $csss)
            {
            foreach ($csss as $c) {
                try {
                    $customer = $c;
                    $segment = Segment::find($customer->segment_id);
                    $customer->hash = uniqid();
                    $duplicate = Customer::where('hash', $customer->hash)->first();
                    if ($segment && !$duplicate) {
                        $entry = new Customer();
                        $entry->name = $customer->name;
                        $entry->last_name = $customer->last_name;
                        $entry->email = $customer->email;
                        $entry->hash = $customer->hash;
                        $entry->created_at = date('Y-m-d h:i:s');
                        if (isset($customer->form_data)) $entry->form_data = $customer->form_data . '}';
                        if (isset($customer->phone)) $entry->phone = $customer->phone;
                        if (isset($customer->company_name)) $entry->company_name = $customer->company_name;
                        if (isset($customer->segment_id)) $entry->segment_id = $customer->segment_id;
                        $entry->company_id = $segment->company->id;
                        $entry->register_at = $customer->datetime;
                        if (isset($customer->comments)) $entry->comments = $customer->comments;
                        $entry->save();

                        Log::info("Customer: " . print_r($entry->toArray(), true));
                        foreach ($customer->answers as $a) {
                            Log::info($a->value);
                            $answer = new Answer();
                            $answer->question_id = $a->question_id;
                            $answer->raw_answer = $a->value;
                            $answer->answer_at = $a->datetime;
                            $answer->customer_id = $entry->id;
                            $answer->segment_id = $customer->segment_id;
                            if (isset($a->comment)) $answer->comment = $a->comment;
                            $answer->created_at = date('Y-m-d h:i:s');
                            $answer->save();
                        }
                        //$answer->value =
                    }
                } catch (Exception $e) {
                    Log::error("ERROR(postSaving:" . $c . ") :: " . $e->getMessage());
                    $r->status->setStatus(Status::STATUS_ERROR_PROCESO);
                    $r->status->details = "ERROR(postSaving:" . $c . ")";
                }
            }
        }
        } catch (Exception $e) {
            Log::error("ERROR(postSync) :: " . $e->getMessage());
            $r->status->setStatus(Status::STATUS_ERROR_PROCESO);
            $r->status->details = "ERROR(postSync)";
        }

        return Response::json($r);
    }*/
	//Segments
	public function putSegment()
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$data=Input::all();		
		$r = new ApiResponse();
		$error = false;

		if (Input::has('id')) {
			$s = Segment::find(Input::get('id'));
			if ($s->company_id != Auth::user()->company->id) {
				$error = true;
				$r->status->setStatus(Status::STATUS_ACCESS_DENIED);
			}
		} else {
			$s = new Segment();
			$s->company_id = Auth::user()->company->id;
		}

		if (!$error) {
			if (Input::has('name')) $s->name = Input::get('name');
			if (Input::has('description')) $s->description = Input::get('description');
			if (Input::has('password')) $s->password = Input::get('password');
			(Input::has('has_form'))? $s->has_form = Input::get('has_form'):$s->has_form ='0';

			if (Input::has('survey_id')) {
				if (Input::get('survey_id') != "-1") {
					$survey = Survey::find(Input::get('survey_id'));
					if ($survey)
						$s->survey()->associate($survey);
				} else {
					$s->survey_id = null;
				}
			}

			$s->save();

			if (Input::has('devices_selection')) $s->devices()->sync(Input::get('devices_selection'));

			$r->data = $s->toArray();
		}

		return Response::json($r);
	}

	public function deleteSegment($_id)
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$r = new ApiResponse();

		$s = Segment::find($_id);
		if ($s) $s->delete();
		$r->data = $s->toArray();

		return Response::json($r);
	}

	public function getSegment($_id = null)
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$r = new ApiResponse();
		$company = Auth::user()->company;
		if ($_id != null) {
			$segment = Segment::find($_id)->load('survey.questions', 'devices');
			if ($segment->company_id == $company->id)
				$r->data = $segment->toArray();
		} else {
			$r->data = Auth::user()->company->segments->load('survey.questions', 'devices')->all();
		}
		return Response::json($r);
	}

	//Questions
	public function putQuestion()
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$r = new ApiResponse();
		$error = false;

		if (Input::has('id')) {
			$s = Question::find(Input::get('id'));
			if ($s->company_id != Auth::user()->company->id) {
				$error = true;
				$r->status->setStatus(Status::STATUS_ACCESS_DENIED);
			}
		} else {
			$s = new Question();
			$s->company_id = Auth::user()->company->id;
		}

		if (!$error) {
			if (Input::has('question')) $s->question = Input::get('question');
			if (Input::has('type')) $s->type = Input::get('type');
			if (Input::has('options')) $s->options = Input::get('options');
			if (Input::has('parent_id')) $s->parent_id = Input::get('parent_id');
			if (Input::has('survey_id')) $s->survey_id = Input::get('survey_id');
			if (Input::has('has_comment')) $s->has_comment = Input::get('has_comment');

			$s->save();
			$r->data = $s->toArray();
		}

		return Response::json($r);
	}

	public function deleteQuestion($_id)
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$r = new ApiResponse();

		$s = Question::find($_id);
		if ($s) $s->delete();
		$r->data = $s->toArray();

		return Response::json($r);
	}

	/**
	 * Function postFind
	 * this function is deprecated
	 */
	public function postFind()
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$r = new ApiResponse();
		$result = array();
		$resultt = array();
		//$segmentId = Input::get('segment_id');
		$segmentId = explode(',', Input::get('segment_id'));
		$startDate = new DateTime(Input::get('start_date'));
		$endDate = new DateTime(Input::get('end_date'));
		$ok = false;
		$datSurveys = array();
		if (is_array($segmentId)) {
			$resultt = array_filter($segmentId);
			$datSurveys = DB::table('segments')->select('survey_id')->whereIn('id', $resultt)->get();
		}

		$idSuverys = array();
		foreach ($datSurveys as $s) {
			array_push($idSuverys, $s->survey_id);
		}
		//$item=$questSurveys->get()->toArray();
		$item = DB::table('questions')->whereIn('survey_id', $idSuverys)->get();

		//if($segmentId != null)
		if ($item) {

			foreach ($item as $s) {
				$questions = (array)$s;
				$startDate = new DateTime(Input::get('start_date'));
				$endDate = new DateTime(Input::get('end_date'));
				$questions['answers'] = Answer::where('question_id', '=', $questions['id'])->where('created_at', '>=', $startDate->format('Y-m-d H:i:s'))->where('created_at', '<=', $endDate->format('Y-m-d H:i:s'))->get()->toArray();
				$result[] = $questions;
				//$r->data = $surveyQuestions->toArray();
				//$r->status->setStatus(Status::STATUS_ACCESS_DENIED);
			}
			$r->data = $result;
		} else {
			$r->data = array();
		}
		return Response::json($r, $r->status->code);
	}

	//Surveys
	public function getSurvey($_id = null)
	{
		$r = new ApiResponse();
		$company = Auth::user()->company;
		if ($_id != null) {
			$survey = Survey::find($_id);
			if ($survey) {
				$survey->load('questions');
				if ($survey->company_id == $company->id) $r->data = $survey->toArray();
				else                                    $r->status->setStatus(Status::STATUS_ACCESS_DENIED);
			}
		} else {
			$r->data = Auth::user()->company->surveys->load('questions')->all();
		}
		return Response::json($r);
	}

	public function putSurvey()
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$r = new ApiResponse();
		$error = false;

		if (Input::has('id')) {
			$s = Survey::find(Input::get('id'));
			if ($s->company_id != Auth::user()->company->id) {
				$error = true;
				$r->status->setStatus(Status::STATUS_ACCESS_DENIED);
			}
		} else {
			$s = new Survey();
			$s->company_id = Auth::user()->company->id;
		}

		if (!$error) {
			if (Input::has('name')) $s->name = Input::get('name');
			if (Input::has('description')) $s->description = Input::get('description');

			$s->save();
			$r->data = $s->toArray();
		}

		return Response::json($r);
	}

	public function deleteSurvey($_id)
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$r = new ApiResponse();

		$s = Survey::find($_id);
		if ($s) $s->delete();
		$r->data = $s->toArray();

		return Response::json($r);
	}

	//Answers
	public function postAnswer()
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$r = new ApiResponse();

		$answersGroupByDeviceSeg = array();
		$questionsGroup = array();
		$countEncuesta = array();
		$segmentsSelected = json_decode(Input::get('segments'));//array into necesary  and device is segment

		if (Auth::user()->hasRole('Admin')) {
			$flag = true;
		} else {
			$dataexist = DB::table('customers')->where('customers.company_id', '=', Auth::user()->company->id)->get();
			if (count($dataexist) > 0) {
				$flag = true;
			}
		}
		if ($flag) {
			if (is_array($segmentsSelected)) {
				foreach ($segmentsSelected as $k=> $seg) {

					$startDate = new DateTime(Input::get('start_date'));
					$endDate = new DateTime(Input::get('end_date'));
					$deviceId = $seg->device_id;
					$answercusto = DB::table('customers')->where('customers.segment_id', '=', $seg->segment_id)/*->where('customers.place_id','=',$seg->place_id)*/
					->join('answers', function ($join) use ($startDate, $endDate) {
						$join->on('customers.id', '=', 'answers.customer_id')->where('answers.answer_at', '>=', $startDate->format('Y-m-d H:i:s'))->where('answers.answer_at', '<=', $endDate->format('Y-m-d 23:59:59'));
					})->get();

					$tempAnswergroup[$seg->device_id] = $answercusto;/*->orderBy('answers.answer_at', 'asc')->get();*/
					array_push($answersGroupByDeviceSeg, $tempAnswergroup);

					$tempEncuesta = DB::table('customers')->where('customers.segment_id', '=', $seg->segment_id)->where('customers.company_id', '=', $seg->company_id)->where('customers.register_at', '>=', $startDate->format('Y-m-d H:i:s'))->where('customers.register_at', '<=', $endDate->format('Y-m-d 23:59:59'))->count();

					array_push($countEncuesta, $tempEncuesta);

					$temp = DB::table('device_segment')->where('device_segment.device_id', '=', $deviceId)->where('device_segment.segment_id', '=', $seg->segment_id)
						//$device = DB::table('device_segment')->where('device_segment.device_id','=',$deviceId)->where('device_segment.segment_id','=',$seg->segment_id)
						->join('segments', 'device_segment.segment_id', '=', 'segments.id')
						->join('surveys', 'segments.survey_id', '=', 'surveys.id')
						->join('questions', 'surveys.id', '=', 'questions.survey_id')->get();
					array_push($questionsGroup, $temp);
					unset($tempAnswergroup[$seg->device_id]);
				}

				$averageGroup = array();
				$averageGroupSeg = array();
				$rptabyQuestion = array();
				$numberQuestions = array();
				$numberQuestionsSeg = array();
				//return $answersGroupByDeviceSeg;
				foreach ($answersGroupByDeviceSeg as $seg => $obj) {

					foreach ($obj as $d => $v) {

							$startDate = new DateTime(Input::get('start_date'));
							$endDate = new DateTime(Input::get('end_date'));
							$averageGroup[$d] = KnowitMaths::getAnswerAveragePerDate($v, $startDate, $endDate);
							$averageGroupSeg[$seg] = $averageGroup;

							$numberQuestions[$d] = count($v);
							$numberQuestionsSeg[$seg] = $numberQuestions;
							foreach ($v as $ans) {
								$rptabyQuestion[] = $ans;
							}
							unset($averageGroup[$d]);
					}
				}

				//return $averageGroupSeg;
				$questionsGroupFinal = array();
				foreach ($questionsGroup as $sg) {
					foreach ($sg as $q) {
						$q->answers = array();
						$questionsGroupFinal[$q->id] = $q;
					}
				}
				$count = 0;
				foreach ($rptabyQuestion as $clave => $valor) {
					if (isset($questionsGroupFinal[$valor->question_id])) {
						$questionsGroupFinal[$valor->question_id]->answers[] = $valor;
					}
					$count++;
				}
				$div = 0;
				$averageBydate = array();
				//return $averageGroupSeg;
				foreach ($averageGroupSeg as $key=> $averageGroup) {
					$div += count($averageGroup);
					foreach ($averageGroup as $g => $v) {
						foreach ($v as $c => $j) {
							if ($j['average'] == 0) $j['average'] = 10;
							if (isset($averageBydate[$c])) {
								$averageBydate[$c]['average'] = $averageBydate[$c]['average'] + floatval($j['average']);
								$averageBydate[$c]['date'] = $j['date'];
							} else {
								$averageBydate[$c]['average'] = array();
								$averageBydate[$c]['average'] = floatval($j['average']);
								$averageBydate[$c]['date'] = $j['date'];
							}
						}
						//var_dump(array_search('average', $v));
					}
				}

				$averageFinal = 0;
				$cc = 0;
				$finalAverage = array();
				foreach ($averageBydate as $t => $av) {
					$avetemp = floatval($av['average'] / $div);

					if (!($avetemp == 10)) {
						$averageFinal = $averageFinal + $avetemp;
						$cc++;
					}
					$dt = new DateTime($av['date']);
					$tempDat['date'] = $dt->format("Y-m-d");
					$tempDat['value'] = $avetemp;
					//$averageBydate[]['date']=$av['date'];
					//$averageBydate[$t]['average']=$avetemp;
					$finalAverage[] = $tempDat;
				}

				$encuestaTotal = 0;
				foreach ($countEncuesta as $v) {
					$encuestaTotal += $v;
				}

				$r->setData($finalAverage);
				$r->customers = $encuestaTotal;

				$scoreKnowit = 10;

				if ($averageFinal != 0)
					//$scoreKnowit = ($averageFinal*100/20);
					$scoreKnowit = $averageFinal / $cc;

				$scoreKnowit = round(($scoreKnowit*100)/20);
				$rpta = 0;

				foreach ($numberQuestionsSeg as $numberQ) {
					foreach ($numberQ as $a) {
						$rpta += $a;
					}
				}

				$typeQuestionBool = DB::table('questions')->select('id')->where('type', '=', 'boolean')->get();
				$typeQuestionOptions = DB::table('questions')->select('id')->where('type', '=', 'options')->get();
				$IdBool = array();
				foreach ($typeQuestionBool as $question) {
					$IdBool[$question->id] = $question->id;
				}
				foreach ($typeQuestionOptions as $options) {
					$IdBool[$options->id] = $options->id;
				}
				//$r->questions = $questionsGroup;
				$r->numberAnswer = $rpta;
				$r->averageFinal = $averageFinal;
				$r->cc = $cc;
				$r->scoreKnowit = $scoreKnowit;
				$r->boolQuestions = $IdBool;
				$r->questionCircle = $questionsGroupFinal;
			}
		}

		return Response::json($r, $r->status->code);
	}

	/**
	 * Function postAnswer2
	 * this function is deprecated
	 */
	public function postAnswer2()
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));

		$r = new ApiResponse();
		$rtaGroup = array();
		$groupAveragebySegment = array();
		$groupNumberAnswerFirst = array();
		$startDate = new DateTime(Input::get('start_date'));
		$endDate = new DateTime(Input::get('end_date'));
		$segmentId = explode(',', Input::get('segment_id'));
		$devices2 = null;
		if (is_array($segmentId)) {
			$result = array_filter($segmentId);
			foreach ($result as $seg_id => $v) {
				$startDate = new DateTime(Input::get('start_date'));
				$endDate = new DateTime(Input::get('end_date'));
				$answer = DB::table('answers')->where('segment_id', '=', $v)->where('created_at', '>=', $startDate->format('Y-m-d H:i:s'))->where('created_at', '<=', $endDate->format('Y-m-d H:i:s'))->orderBy('created_at', 'asc')->get();
				$groupAveragebySegment[$v] = KnowitMaths::getAnswerAveragePerDate($answer, $startDate, $endDate); //old v
				$groupNumberAnswerFirst[$v] = KnowitMaths::getNumberAnswerFirstQuestion($v, $startDate, $endDate);//change $d_id by $segmentId
			}
			$m_questions = DB::table('questions')->select('id');
			$m_questions->Where('type', '=', 'boolean');
			$devices2 = $m_questions->get();
		} else {

		}
		/*    $dataFinalAverage=array();
			$ccoun=array();
			foreach($groupAveragebySegment as $seg_id => $v ){
				if(isset($dataFinalAverage[$v])){
						$ccoun[$v]++;
						$dataFinalAverage[$v]['average']=$dataFinalAverage[$v]['average']+$v->average;
						//$dataFinalAverage[$v]['average'] = floatval($dataFinalAverage[$v]['average'])/$ccoun[$v];
				}else{
					$dataFinalAverage[$v]=$v;
					$ccoun[$v]=1;
				}
			}/**/

		/*  $devices2=null;
			if($segmentId != null)
			{
				$answer = Answer::where('segment_id', '=', $segmentId)->where('created_at', '>=', $startDate->format('Y-m-d H:i:s'))->where('created_at', '<=', $endDate->format('Y-m-d H:i:s'))->orderBy('created_at', 'asc')->get();
				$m_questions = DB::table('questions')->select('id');
				$m_questions->Where('type', '=', 'boolean');
				$devices2 = $m_questions->get();
			}else{
				//$r->data = array();
			}/**/

		/*$r->setData(KnowitMaths::getAnswerAveragePerDate($answer->get(), $startDate, $endDate)); //old v
		$startDate = new DateTime(Input::get('start_date'));
		$endDate = new DateTime(Input::get('end_date'));/**/

		$r->setData($groupAveragebySegment);
		$r->typeDev = $devices2;
		$r->countt = $groupNumberAnswerFirst;//change $d_id by $segmentId
		//$r->countt=KnowitMaths::getNumberAnswerFirstQuestion($result,$startDate, $endDate);//change $d_id by $segmentId
		//$r->ssqql= $startDate;
		/**/
		//$r,$r->status->code
		return Response::json($r, $r->status->code);
	}


	public function postDelete()
	{
		Log::info('INFO::HTTP',array($_SERVER['REQUEST_METHOD']));
		Log::info('INFO::PARAMETER',Input::all());
		Log::info('INFO::METHOD',array(Request::url()));
		
		$r = new ApiResponse();
		if(Input::has('id'))
		{
			$_id=Input::get('id');
			$s = Device::find($_id);
			if($s) $s->delete();
			$r->data = $s->toArray();
		}
		else
		{
			$r->status->code="220";
			$r->status->description="Faltan parametros";
		}
		return Response::json($r);
	}



}
