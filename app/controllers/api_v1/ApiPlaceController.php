<?php

class ApiPlaceController extends BaseController {

	public function getIndex()
	{
		$r = new ApiResponse();
        /*if(Auth::user()->hasRole('Admin')){
            $places = Place::all();//has('devices')->get();
        }/**/
		$places = Auth::user()->company->places->load('devices.segments');

		$r->data = $places;

		return Response::json($r);
	}

	//Segments
	public function putPlace()
	{
		$r = new ApiResponse();
		$error = false;

		if(Input::has('id')){
			$s = Place::find(Input::get('id'));
			if($s->company_id != Auth::user()->company->id){
				$error = true;
				$r->status->setStatus(Status::STATUS_ACCESS_DENIED);
			}
		} else {
			$s = new Place();
			$s->company_id = Auth::user()->company->id;
		}
		if(!$error)
		{
			if(Input::has('name')) $s->name = Input::get('name');
			if(Input::has('description')) $s->description = Input::get('description');
			if(Input::has('lat')) $s->lat = Input::get('lat');
			if(Input::has('lon')) $s->lon = Input::get('lon');

			$s->save();
			$r->data = $s->toArray();
		}

		return Response::json($r);
	}

	public function deletePlace($_id)
	{
		$r = new ApiResponse();

		$s = Place::find($_id);
		if($s) $s->delete();
		$r->data = $s->toArray();

		return Response::json($r);
	}


	public function postDelete()
	{
		$r = new ApiResponse();
		if(Input::has('id'))
		{
			$_id=Input::get('id');
			$s = Place::find($_id);
			if($s) $s->delete();
			$r->data = $s->toArray();
		}
		else
		{
			$r->status->code="220";
			$r->status->description="Faltan parametros";
		}


		return Response::json($r);
	}

}
