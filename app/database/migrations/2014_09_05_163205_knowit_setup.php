<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KnowitSetup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		/**
		 * Model: Image
		 * Permite llevar un registro de las imagenes que se usarán para personalizar una compañia, y poder reutilizarla o modificarla de ser necesario.
		 * La imagen le pertener al usuario que la cargo pero el administrador puede acceder a cualquiera.
		 */
		Schema::create('images', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('path');
			$table->integer('user_id')->nullable()->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
			$table->timestamps();
		});

		/**
		 * Model: Company
		 * Almacena información de una empresa, está relacionada con un usuario owner o propietario que es usado par controlar el periodo de subscripción
		 */
		Schema::create('companies', function($table)
		{
			$table->increments('id');
			$table->integer('image_id')->unsigned()->nullable();
			$table->foreign('image_id')->references('id')->on('images')->onDelete('SET NULL');
			$table->integer('user_id')->nullable()->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->string('name');
			$table->string('description');
			$table->timestamps();
			$table->softDeletes();
		});

		/**
		 * Model: Place
		 * Almacena información de una empresa, está relacionada con un usuario owner o propietario que es usado par controlar el periodo de subscripción
		 */
		Schema::create('places', function($table)
		{
			$table->increments('id');
			$table->integer('company_id')->nullable()->unsigned();
			$table->foreign('company_id')->references('id')->on('companies');
			$table->string('name');
			$table->string('lat');
			$table->string('lon');
			$table->string('description');
			$table->timestamps();
		});

		/**
		 * Model: Form
		 * Almacena las respuestas generadas en lso dispositivos, tiene información del momento y el lugar donde se hizo la respuesta
		 */
		Schema::create('subscriptions', function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->text('schema');
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
			$table->timestamps();
		});

		/**
		 * Model: Survey
		 * Grupo de preguntas que se pueden asignar a multiples dispositivos.
		 */
		Schema::create('surveys', function($table)
		{
			$table->increments('id');
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
			$table->string('name');
			$table->string('description');
			$table->string('type');
			$table->timestamps();
			$table->softDeletes();
		});

		/**
		 * Model: Device
		 * Dispositivos físico disponible para mostrar encuestas, también almacena la útlima ubicación registrada de la tableta.
		 */
		Schema::create('devices', function($table)
		{
			$table->increments('id');
			$table->integer('company_id')->unsigned()->nullable();
			$table->foreign('company_id')->references('id')->on('companies')->onDelete('SET NULL');
			$table->integer('place_id')->unsigned()->nullable();
			$table->foreign('place_id')->references('id')->on('places')->onDelete('SET NULL');
			$table->string('uid')->unique();
			$table->string('type', 10);
			$table->string('name');
			$table->string('alias');
			$table->string('lat', 30);
			$table->string('lon', 30);
			$table->smallInteger('disabled');
			$table->timestamps();
		});

		/**
		 * Model: Segment
		 * Identificador individual de las respuestas del cliente
		 */
		Schema::create('segments', function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->string('description');
			$table->string('password');
			$table->integer('order')->unsigned();
			$table->tinyInteger('has_form')->nullable();
			$table->integer('survey_id')->unsigned()->nullable();
			$table->foreign('survey_id')->references('id')->on('surveys')->onDelete('SET NULL');
			$table->integer('subscription_id')->unsigned()->nullable();
			$table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('SET NULL');
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
			$table->timestamps();
		});

		/**
		 * Model: Device Segment
		 * Identificador individual de las respuestas del cliente
		 */
		Schema::create('device_segment', function($table){
			$table->integer('device_id')->unsigned()->nullable();
			$table->foreign('device_id')->references('id')->on('devices')->onDelete('cascade');
			$table->integer('segment_id')->unsigned();
			$table->foreign('segment_id')->references('id')->on('segments')->onDelete('cascade');
			$table->integer('order')->unsigned();
			$table->timestamps();
		});

		/**
		 * Model: Question
		 * Indicador de evaluación, almacena la pregunta y las opciones de respuesta según el tipo.
		 * Los tipos pueden ser: default, custom, options, boolean, comment
		 */
		Schema::create('questions', function($table){
			$table->increments('id');
			$table->integer('company_id')->unsigned()->nullable();
			$table->foreign('company_id')->references('id')->on('companies')->onDelete('SET NULL');
			$table->integer('parent_id')->unsigned()->nullable();
			$table->foreign('parent_id')->references('id')->on('questions')->onDelete('cascade');
			$table->integer('survey_id')->unsigned();
			$table->foreign('survey_id')->references('id')->on('surveys')->onDelete('cascade');
			$table->string('question');
			$table->smallInteger('has_comment')->unsigned();
			$table->string('type');
			$table->integer('order')->unsigned();
			$table->string('options'); //Para guardr en json las opciones disponibles para esta pregunta
			$table->timestamps();
		});


		/**
		 * Model: Customer
		 * Identificador individual de las respuestas del cliente
		 */
		Schema::create('customers', function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->string('phone');
			$table->string('address');
			$table->string('last_name');
			$table->integer('segment_id')->unsigned();
			$table->text('comments')->nullable();
			$table->foreign('segment_id')->references('id')->on('segments')->onDelete('cascade');
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
			$table->integer('place_id')->unsigned()->nullable();
			$table->foreign('place_id')->references('id')->on('places')->onDelete('SET NULL');
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
			$table->integer('subscription_id')->unsigned()->nullable();
			$table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('SET NULL');
			$table->text('form_data');
			$table->timestamp('register_at');
			$table->timestamp('created_at');
		});

		/**
		 * Model: Answer
		 * Almacena las respuestas generadas en lso dispositivos, tiene información del momento y el lugar donde se hizo la respuesta
		 */
		Schema::create('answers', function($table)
		{
			$table->increments('id');
			$table->integer('segment_id')->unsigned();
			$table->foreign('segment_id')->references('id')->on('segments')->onDelete('cascade');
			$table->integer('customer_id')->unsigned();
			$table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
			$table->integer('question_id')->unsigned();
			$table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
			$table->string('raw_answer', 350);
			$table->text('comment');
			$table->string('lat', 30);
			$table->string('lon', 30);
			$table->timestamp('answer_at');
			$table->timestamp('created_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
