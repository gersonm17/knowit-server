<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeopleColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->integer('image_id')->unsigned()->nullable();
			$table->foreign('image_id')->references('id')->on('images');
			$table->integer('company_id')->unsigned()->nullable();
			$table->foreign('company_id')->references('id')->on('companies');
			$table->string('name');
			$table->string('last_name');
			$table->string('dni')->unique()->nullable();
			$table->string('phone');
			$table->date('birth_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
