<?php

class DatabaseSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		/*Roles */

		Role::create(array('name' => 'Admin'));
		Role::create(array('name' => 'Propietario'));
		Role::create(array('name' => 'Encargado'));
		Role::create(array('name' => 'Analista'));
		Role::create(array('name' => 'Colaborador'));
		Role::create(array('name' => 'Usuario'));

		$user = new User;
		$user->username = 'admin';
		$user->email = 'admin@knowit.pe';
		$user->password = '123456';
		$user->password_confirmation = '123456';
		$user->confirmed = 1;
		$user->name = 'Gerson';
		$user->last_name = 'Aduviri';
		$user->confirmation_code = md5(uniqid(mt_rand(), true));
		$user->save();

		$user1 = new User;
		$user1->username = 'movistar';
		$user1->email = 'rloayza@knowit.pe';
		$user1->password = '123456';
		$user1->password_confirmation = '123456';
		$user1->confirmed = 1;
		$user1->name = 'Rafael';
		$user1->last_name = 'Loayza';
		$user1->confirmation_code = md5(uniqid(mt_rand(), true));
		$user1->save();

		Company::create(array('name' => 'Geek Advice',
			'description' => 'Empresa de desarrollo',
			'user_id' => 1,
			'created_at' => '2014-10-10 10:10:10',
			'updated_at' => '2014-10-10 10:10:10'
		));

		Company::create(array('name' => 'Movistar',
			'description' => 'Servicio al cliente',
			'user_id' => 2,
			'created_at' => '2014-10-10 10:10:10',
			'updated_at' => '2014-10-10 10:10:10'
		));

		Device::create(array('uid' => 'TESTDEVICE001', 'name' => 'Lenovo 7" negra', 'company_id' => 1,));
		Device::create(array('uid' => 'WEBDEVICE001', 'name' => 'http://geekadvice.pe', 'company_id' => 1, 'type' => 'web'));
		Device::create(array('uid' => 'TESTDEVICE002', 'name' => 'Lenovo Yoga 10"', 'company_id' => 1,));
		Device::create(array('uid' => 'TESTDEVICE003', 'name' => 'MTV blanca 10" a', 'company_id' => 1,));
		Device::create(array('uid' => 'TESTDEVICE004', 'name' => 'MTV blanca 10" b', 'company_id' => 2,));
		Device::create(array('uid' => 'WEBDEVICE002', 'name' => 'formax.pe', 'company_id' => 2, 'type' => 'web'));

		Place::create(array('name' => 'Oficina Arequipa', 'company_id' => 1));
		Place::create(array('name' => 'Oficina Lima', 'company_id' => 1));
		Place::create(array('name' => 'Tienda Jockey Plaza', 'company_id' => 2));
		Place::create(array('name' => 'Oficina Javier Prado', 'company_id' => 2));
		Place::create(array('name' => 'Tienda La Rambla', 'company_id' => 2));
		Place::create(array('name' => 'Tienda MegaPlaza', 'company_id' => 2));
		Place::create(array('name' => 'Tienda Plaza Lima Norte', 'company_id' => 2));
		Place::create(array('name' => 'Tienda Villa El Salvador', 'company_id' => 2));

		$user->attachRole(1);
		$user1->attachRole(2);

		$user = User::find(1);
		$user->company_id = 1;
		$user->save();

		$user1 = User::find(2);
		$user1->company_id = 2;
		$user1->save();
	}

}
