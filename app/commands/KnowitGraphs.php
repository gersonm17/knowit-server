<?php
/**
 * Created by JetBrains PhpStorm.
 * User: GersonM
 * Date: 30/11/12
 * Time: 06:53 PM
 * To change this template use File | Settings | File Templates.
 */
class KnowitGraphs
{
	const DATE_FORMAT = 'd-m-Y H:i';
	const DATE_FORMAT_SYSTEM = 'Y-m-d H:i:s';

	public function __construct()
	{

	}

	static function makeChart($_title, $_answers, $_width = 900, $_height = 500)
	{
		JpGraph\JpGraph::load();
		JpGraph\JpGraph::module('line');
		JpGraph\JpGraph::module( "date" );
		JpGraph\JpGraph::module( "bar" );

		$graph = new Graph($_width,$_height);

		$graph->SetScale("datlin");

		$graph->SetTheme(new UniversalTheme());
		$graph->title->Set($_title);
		$graph->SetBox(false);

        $data1y=array(57,90,40,116);
        // add barplot
        $result = array();
		$dates = array();

		foreach($_answers as $a)
		{
			if($a['average']==0) $result[] = 10;
            else $result[] = $a['average'];
            //$result[] = $a['average'];
			$dates[] = strtotime($a['date']);
		}

		$p1 = new LinePlot($result, $dates);
		//$p1->SetColor("#6495ED");
        $p1->mark->SetType(MARK_FILLEDCIRCLE);


        $p1->mark->SetFillColor('#71529D');
        $p1->SetColor("#6495ED");
		//$p1->SetLegend('Promedio de aprobación');
		//$p1->mark->SetType(MARK_SQUARE);

		$graph->Add($p1);
		$graph->SetTickDensity( 2, 4 );
		$graph->xaxis->scale->SetDateFormat( 'd/m' );

		$file_name = 'temp/chart_'.uniqid().'.jpg';

		$graph->Stroke($file_name);

		return $file_name;
	}

	static function makePie($_type, $_answers, $_width = 450, $_height = 450)
	{
		JpGraph\JpGraph::load();
		JpGraph\JpGraph::module( "pie" );
		JpGraph\JpGraph::module( "pie3d" );
       // JpGraph\JpGraph::module("iconPlot");
        JpGraph\JpGraph::module("line");
        JpGraph\JpGraph::module("canvas");

		$graph = new PieGraph($_width, $_height);
       // $_answers = array(29, 0, 0,16,0);//test
        if($_type=='boolean'){
            $color=array('#00A194','#71529D','','');
        //}else $color=array('#3A6DB5','#8D4F9F','#F5A71B','#72B33C');//malo,regular, bueno, exelent
        }else $color=array('#71529D','#8974A6','#59BBB3','#00A194');


        $_answers_soft=array();
        $_answers_soft['1']=$_answers[0];
        $_answers_soft['2']=$_answers[1];
        $_answers_soft['3']=$_answers[2];
        $_answers_soft['4']=$_answers[3];

        $answers_sin=array_values(array_diff($_answers_soft, array('0')));

        $colorFilter=array();
        $cont1=0;
        $overflow=false;
            foreach ($_answers as $key => $val) {
                if($val!=0){
                     array_push($colorFilter,$color[$key]);
                }
                if($val==1)$cont1++;
                if($val>=11)$overflow=true;
            }
        //var_dump($colorFilter);
        $colorFilter2 = array_values(array_diff($colorFilter, array('')));

        if(!empty($answers_sin)){
            $p1= new PiePlot3d($answers_sin);
            $p1->value->SetColor('white','');
            $p1->value->SetFont(FF_DEFAULT,FS_NORMAL,$aSize=10);
            if($cont1>=2||($overflow==true))$p1->SetLabelMargin(-10);
            else $p1->SetLabelMargin(-55);

            $p1->SetAngle(59);
            //$p1->ShowBorder();
            $p1->SetHeight(15);
            $p1->SetLabelPos(0.7);
            $graph->Add($p1);
            $p1->SetColor('black');
            $p1->SetSliceColors($colorFilter2);
        }else{
            $graph->title->SetPos(5,50);
            $graph->title->Set('No hay datos disponibles');
        }

        $graph->title->SetMargin(10);
        $graph->title->Align(1,'top','j');
        $graph->title->SetScalePos(100,20);
        $graph->title->SetParagraphAlign(5);
        $graph->title->SetFont(FF_DEFAULT,FS_NORMAL,$aSize=11);
        $graph->title->SetColor(array(178,178,177));
       // $graph->title->SetBox(array(255,255,255),array(0,0,0),false,0,20);
		$graph->title->iWordwrap = 0;

        //$p1->SetColor('black');
        //$p1->SetSliceColors(array('#3A6DB5','#8D4F9F','#F5A71B','#72B33C'));

		$file_name = 'temp/pie_'.uniqid().'.jpg';
		$graph->Stroke($file_name);
        //echo $file_name;
		return $file_name;
	}

	public static function cleanTemps()
	{
		//TODO: Limpiar los gráficos que se generan en temps
		//TODO: Es importante, tienes que limpiar estos archivos
		//TODO: Implementar clase de mantenimiento para que el admin pueda limpiar este tipo de cosas
	}
}