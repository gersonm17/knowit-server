<?php
/**
 * Created by JetBrains PhpStorm.
 * User: GersoM
 * Date: 30/11/12
 * Time: 06:53 PM
 * To change this template use File | Settings | File Templates.
 */
class KnowitMaths
{
    const DATE_FORMAT = 'd-m-Y H:i';
    const DATE_FORMAT_SYSTEM = 'Y-m-d H:i:s';
    const ACTIVE = 1;
    const INACTIVE = 2;
    const LOCKED = 3;

    public function __construct()
    {

    }

    static function countAnswersByHour($_answers)
    {
        $answer_count = array();

        for($i = 8; $i <= 22; $i++) $answer_count[$i] = 0;

        foreach($_answers as $a)
        {
            $date = new DateTime($a->created_at);
            $answer_count[$date->format('G')]++;
        }

        return $answer_count;
    }

    static function countAnswersByDay($_answers)
    {
        $answer_count = array();

        for($i = 1; $i <= 31; $i++) $answer_count[$i] = 0;

        foreach($_answers as $a)
        {
            $date = new DateTime($a->created_at);
            $answer_count[$date->format('j')]++;
        }

        return $answer_count;
    }

    static function groupAnswerByType($_answers)
    {
        $answer_count = array();

        foreach($_answers as $a)
        {
            if(!isset($answer_count[$a->raw_answer]))	$answer_count[$a->raw_answer] = array();
            array_push($answer_count[$a->raw_answer], $a);
        }

        return $answer_count;
    }

    static function getAnswerCount($_answers)
    {
        $answer_count = array();
        $prefix = 't_';
        //$total = count($_answers);

        foreach($_answers as $a)
        {
            if(!isset($answer_count[$prefix . $a->raw_answer]))	$answer_count[$prefix . $a->raw_answer] = 0;
            $answer_count[$prefix . $a->raw_answer]++;
        }

        return $answer_count;
    }

    static function getAnswerAveragePerDate($_answers, DateTime $_initDate = NULL, DateTime $_endDate = NULL)
    {


        $typeQuestionBool=DB::table('questions')->select('id')->where('type', '=', 'boolean')->get();
        $IdBool=array();
        foreach ($typeQuestionBool as $question)
        {
            $IdBool[]=$question->id;
        }

        $formatKey = 'Y-m-d';
        $answer_count = array();

        foreach($_answers as $a)
        {

            $date = new DateTime($a->register_at);
            $day_val = $date->format($formatKey);
            if(!isset($answer_count[$day_val]))
                $answer_count[$day_val] = array();

            array_push($answer_count[$day_val], $a);
        }



        $average_days = array();
        if($_initDate && $_endDate)
        {
            //$_endDate->setTime(23,59,59);
            $interval = new DateInterval('P1D');
//var_dump($_endDate);
//var_dump($_initDate);

            while($_initDate <= $_endDate)
            {
                $currentKey = $_initDate->format($formatKey);
                //var_dump($currentKey);
                //$currentKey = $_initDate;
                //var_dump($currentKey);
                $average_days[$currentKey] = array('answers' => NULL, 'average' => 0, 'date'=>$_initDate->format('Y-m-d h:i:s'));

                if(isset($answer_count[$currentKey]))
                {
                    //var_dump($answer_count[$currentKey]);
                    $average_days[$currentKey]['answers'] = $answer_count[$currentKey];
                    $average_days[$currentKey]['datestrue'] = $_endDate->format('Y-m-d h:i:s');
                    //$average_days[$currentKey]['average'] = 25;
                    $average_days[$currentKey]['average'] = KnowitMaths::getAnswerAverage($answer_count[$currentKey],$IdBool,'true');
                    //$average_days[$currentKey]['date'] = $_initDate->format('Y-m-d\Th:i:s') . '.280Z';

                }

                $_initDate->add($interval);
            }

        }

        return $average_days;
    }

    static function getAnswerAveragePerDay($_answers, $_goods = true)
    {
        $answer_count = array();

        foreach($_answers as $a)
        {
            $date = new DateTime($a->created_at);
            $day_val = intval($date->format('d'));
            if(!isset($answer_count[$day_val]))
                $answer_count[$day_val] = array();
            array_push($answer_count[$day_val], $a);
        }

        $average_days = array();

        for($i=1;$i<=31;$i++)
        {
            if(isset($answer_count[$i]))	$average_days[$i] = KnowitMaths::getAnswerAverage($answer_count[$i], $_goods,'true');
            else							$average_days[$i] = 0;
        }

        return $average_days;
    }

    static function getAnswerAveragePerHour($_answers, $_goods = true)
    {
        $answer_count = array();

        foreach($_answers as $a)
        {
            $date = new DateTime($a->created_at);
            if(!isset($answer_count[$date->format('G')]))
                $answer_count[$date->format('G')] = array();
            array_push($answer_count[$date->format('G')], $a);
        }

        $average_hours = array();

        for($i=8;$i<=22;$i++)
        {
            if(isset($answer_count[$i]))	$average_hours[$i] = KnowitMaths::getAnswerAverage($answer_count[$i], $_goods);
            else							$average_hours[$i] = 0;
        }

        return $average_hours;
    }

    static function getAnswerAverage($_answers,$IdBool=null,$_goods = true)
    {
        $answer_count = array(0,0,0,0,0);
        $percent_result = 0;

        foreach($_answers as $a)
        {
            if($_goods==='true'){
                if(!in_array($a->question_id, $IdBool)){

                    if(isset($answer_count[$a->raw_answer]))	$answer_count[$a->raw_answer]++;
                    else							$answer_count[$a->raw_answer] = 1;
                }
            }else{
                if(isset($answer_count[$a->raw_answer]))	$answer_count[$a->raw_answer]++;
                else							$answer_count[$a->raw_answer] = 1;

            }/**/
        }

        $total = array_sum($answer_count);
        $sum_goods = $answer_count[2] + 2*($answer_count[3]);
        $sum_bads = 2*($answer_count[0]) + $answer_count[1];

        //if($total > 0)	$percent_result = (100/$total) * ($sum_goods - $sum_bads);//old version

        $difer=$sum_goods - $sum_bads;

        $Scale=10;
        $scalingResult=0;
        if($total > 0)	{

            $percent_result = (100/(2*$total))*($difer);
            $scalingResult = ($percent_result*10)/100;

            if($difer<0) {
                $percent_result_temp=$scalingResult;
                if(abs($percent_result_temp)>$Scale) $scalingResult=0;
                else{
                    $scalingResult=$Scale+$scalingResult;
                }
            }
            elseif($difer==0)$scalingResult=10;
            else $scalingResult=$Scale+$scalingResult;

        }
        //return  round($percent_result); //old v
        return  round($scalingResult,1);
    }

    static  function getNumberAnswerFirstQuestion($segmentId,$startDate,$endDate){// $d_id is $segmentId
        $numberAnswers=0;
        //$surveyId=0;
        if(isset($segmentId)){
            //$answer = DB::table('questions')
            $answer = Segment::where('id', '=', $segmentId)->get();
            if(count($answer)>0){
                $surveyId = $answer[0]->survey_id;
                $surQuestions = Question::where('survey_id', '=', $surveyId)->orderBy('order', 'asc')->get();
                //var_dump($surQuestions[0]->id);
                //$item = $surveyQuestions->toArray();¡
                if(isset($surQuestions[0]->id)){
                    $numberAnswers= Answer::where('question_id', '=', $surQuestions[0]->id)->where('answer_at', '>=', $startDate->format('Y-m-d H:i:s'))->where('answer_at', '<=', $endDate->format('Y-m-d H:i:s'))->count();
                }
            }

           /*
            /**/
        }
        return  $numberAnswers;
    }

    static function HumanDate($_time)
    {
        if(is_string($_time))	$elapsed = strtotime($_time);
        else					$elapsed = $_time;

        $dias       = intval((time() - $elapsed) / 86400);
        $segundos   = intval((time() - $elapsed));

        if($dias < 0) {
            return -1; //Error
        } elseif ($segundos==0) {
            $fecha = "En este momento";
        } elseif ($segundos > 0 && $segundos < 60) {
            $fecha = "Hace " . $segundos . " segundos";
        } elseif ($segundos >= 60 && $segundos <120 ) {
            $fecha = "Hace un minuto";
        } elseif ($segundos >= 120 && $segundos < 3600 ) {
            $fecha = "Hace " . intval($segundos/60) . " minutos";
        } elseif ($segundos >= 3600 && $segundos < 7200) {
            $fecha = "Hace una hora";
        } elseif ($segundos >= 7200 && $segundos < 86400) {
            $fecha = "Hace " . intval($segundos/3600) . " horas";
        } elseif ($dias==1) {
            $fecha = "Ayer";
        } elseif ($dias >= 2 && $dias <= 6) {
            $fecha =  "Hace " . $dias . " días";
        } elseif ($dias >= 7 && $dias < 14) {
            $fecha= "La semana pasada";
        } elseif ($dias >= 14 && $dias <= 365) {
            $fecha =  "Hace " . intval($dias / 7) . " semanas";
        } else {
            $fecha = date("d-m-Y", $elapsed);
        }

        return $fecha;
    }
}