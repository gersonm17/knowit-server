<div class="resumen row" ng-controller="ResumenController">
	<div class="container">

		<!-- Primera columna -->

		<div class="medium-3 columns">
			<div class="block-list" ng-repeat="place in places.data">
				<h3 class="title">
					<div class="controls">
						<a ng-click="selectAll(place)" tooltip="Seleccionar todos"><i class="ii-circle-check icon19"></i></a>
						<a ng-click="selectNone(place)" class="icon-anim" ng-show="place.hasSelected" tooltip="Deseleccionar todo"><i class="ii-circle-cross icon19"></i></a>
					</div>
					<i class="ii-directions"></i> {{place.name}}</h3>

				<!-- Lista dispositivos -->
				<div class="list">
					<div class="msg msg-info" ng-show="place.devices.length == 0">No tienes dispositivos en este lugar, para agregarlos ingresa <a href="#/administrador">aquí</a></div>
					<div ng-repeat="device in place.devices">
						<div class="sub-title" tooltip="{{device.name}}">{{device.alias || device.name }}</div>
						<ul class="list-editable">
							<li ng-repeat="s in device.segments" class="list-anim">
								<span tooltip="{{ s.description }}">{{ s.name }}</span>
								<fieldset class="switch round tiny right">
									<input id="segment_{{device.id}}_{{s.id}}" type="checkbox" ng-model="s.selected" ng-change="updateDetail()">
									<label for="segment_{{device.id}}_{{s.id}}"></label>
								</fieldset>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Sección de contenido -->

		<div class="medium-9 columns">
			<div class="block-content content">
				<h2>Resumen del día</h2>

				<div class="resumen-detail">
					<div class="tip" ng-hide="segmentsSelected.length > 0">
						<h3 class="tip-title"><i class="ii-info icon32 right"></i> Elige un segmento</h3>
						<p>Puedes seleccionar uno o más segmentos, luego podrás ver el detalle aquí.</p>
					</div>

					<div ng-show="isLoading">
						Actualizando vista...
					</div>

					<div ng-show="segmentsSelected.length > 0">
						<h3>Segmentos seleccionados: {{ segmentsSelected.length }}</h3>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
