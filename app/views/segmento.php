<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="es"><!--<![endif]-->
<head>
    <meta charset=utf-8 />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Formax &#8211; Estrategias de modelos de negocio online">
    <meta name="description" content="o">
    <title>Knowit &#8211; Segmento</title>

    <!--[if lt IE 9]><script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>	<![endif]-->

    <link type="text/css" href="<?php echo URL::to('css/style.css'); ?>" rel="stylesheet" />
    <link type="text/css" href="<?php echo URL::to('css/sprites.css'); ?>" rel="stylesheet" />

    <link rel="shortcut icon" href="<?php echo URL::to('favicon.png'); ?>">
</head>
<div class="content">
    <div class="row">
        <ul id="list-questions" class="list-items">
            <?php
            foreach($segments as $q)
            {
                echo '<li id="'.$q->id.'"><div class="pull-right"><a target="_blank"style="cursor:pointer" href="'.URL::to('questions/company').'/'.$q->survey_id.'">'. $q->name.'</a> </div></li>';
            }?>
        </ul>
    </div>
</div>