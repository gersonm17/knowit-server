@extends('ci.tpl_base')

@section('content')
    <div class="column medium-4 small-centered">
        <br/>
        <h2 class="text-center">Iniciar sesión</h2>
        <form role="form" method="POST" action="{{ URL::to('/users/login') }}" accept-charset="UTF-8">
            <input type="hidden" name="_token" value="{{ Session::getToken() }}">
            <fieldset class="content">
                <p>
                    <label for="email">Ingresa tu nombre de usuario o E-mail:</label>
                    <input class="form-control" tabindex="1" placeholder="Usuario o nombre de usuario" type="text"
                           name="email" id="email" value="{{ Input::old('email') }}">
                </p>
                <p>
                    <label for="password">Contraseña:</label>
                    <input class="form-control" tabindex="2" placeholder="Contraseña" type="password" name="password"
                           id="password">
                </p>

                <p>
                    <label for="remember">
                        <input type="hidden" name="remember" value="0">
                        <input tabindex="4" type="checkbox" name="remember" id="remember" value="1"> Mantener mi sesión activa
                    </label>
                </p>

                @if (Session::get('error'))
                    <p class="msg-info">{{{ Session::get('error') }}}</p>
                @endif

                @if (Session::get('notice'))
                    <p class="msg-info">{{{ Session::get('notice') }}}</p>
                @endif
                <div class="text-center">
                    <p><button tabindex="3" type="submit" class="button"><i class="ii-lock"></i> Ingresar</button></p>
                    <p><a href="{{{ URL::to('/users/forgot_password') }}}">¿Olvidaste tu contraseña?</a></p>
                </div>
            </fieldset>
        </form>
    </div>
@stop