<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Knowit :. Acceso de clientes</title>
	<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
	<link rel="stylesheet" href="{{ asset('css/sprites.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">


	<link rel="shortcut icon" href="{{ asset('favicon.png') }}">
	<!--<link href='http://fonts.googleapis.com/css?family=Slabo+27px|Roboto+Condensed:300' rel='stylesheet' type='text/css'>-->

	<!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,500|Roboto+Condensed:300' rel='stylesheet' type='text/css'>-->
</head>

<body>
<br/>
	<div class=" medium-3 large-3 small-centered columns">
		<div class="logo">
			<object type="image/svg+xml" data="<?php echo URL::to('/svg_sprites/logo_knowit.svg'); ?>">
				<img src="<?php echo URL::to('/svg_sprites/logo_knowit.png'); ?>" alt=""/>
			</object>
		</div>
	</div>


	<!-- End popup -->

	<div id="content">
		<?php if(isset($content)) echo $content; ?>
		@yield('content')
	</div>
	<div id="footer">
		<p class="text-center">Knowit - Todos los derechos reservados</p>
	</div>

	<script src="{{ asset('js/libs/jquery-1.10.2.js') }}"></script>
</body>
</html>