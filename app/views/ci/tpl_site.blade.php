<!DOCTYPE html>
<html lang="es" ng-app="KnowitApp">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Knowit :. {{ Auth::user()->company->name; }}</title>
	<link rel="stylesheet" href="{{ asset('packages/loading_bar/loading-bar.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/select/angular-multi-select.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/pickadate/angular-pickadate.css') }}">
	<link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
	<link rel="stylesheet" href="{{ asset('css/sprites.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="shortcut icon" href="{{ asset('favicon.png') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
	<!--<link href='http://fonts.googleapis.com/css?family=Slabo+27px|Roboto+Condensed:300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>-->

</head>

<body>
	<span class="app-version" app-version ></span>

    <ul class="f-dropdown" id="list-companies">
        <li><a href="#">Empresa</a></li>
        <li><a href="#">Empresa</a></li>
        <li><a href="#">Empresa</a></li>
        <li><a href="#">Empresa</a></li>
        <li><a href="#">Empresa</a></li>
        <li><a href="#">Empresa</a></li>
        <li><a href="#">Empresa</a></li>
    </ul>

	<div id="header">
		<div class="row">
			<div class="right">
				<a href="{{ URL::to('users/logout') }}">Cerrar sesión <span class="ii-power"></span></a>
			</div>
			<div>
				Bienvenido <a href="#/profile">{{ Auth::user()->name }}</a> |
                <a dropdown-toggle="#list-companies" class="dropdown">{{ Auth::user()->company->name }}</a>
			</div>
		</div>
	</div>

	<!-- End header -->

	<div class="row" id="home" ng-controller="MenuController">
		<div class="right hidden-for-small-down" ng-hide="!isSmall()">
			<div class="logo" style="width: 150px;">
				<object type="image/svg+xml" data="<?php echo URL::to('/svg_sprites/logo_knowit.svg'); ?>">
					<img src="<?php echo URL::to('/svg_sprites/logo_knowit.png'); ?>" alt=""/>
				</object>
			</div>
		</div>

		<div class="small-menu clearfix text-center" ng-hide="!isSmall()">
			<ul>
				<li class="resumen"><a href="#/resumen" ng-class="isActive('/resumen')"><i class="ii-calendar icon23"></i><br/>Resumen</a></li>
				<li class="administrador"><a href="#/administrador" ng-class="isActive('/administrador')"><i class="ii-mobile2 icon23"></i><br/>Administrador</a></li>
				<li class="reportes"><a href="#/reportes" ng-class="isActive('/reportes')"><i class="ii-stats-bars icon23"></i><br/>Reportes</a></li>
				<li class="base-datos"><a href="#/base-datos" ng-class="isActive('/base-datos')"><i class="ii-vcard icon23"></i><br/>Base de datos</a></li>
                <!-- admin -->
                <?php if(Auth::user()->hasRole('Admin')){ ?>
				<li class="volver"><a href="#/knowit" ng-class="isActive('/knowit')"><i class="ii-big_face icon23"></i><br/>Knowit</a></li>
                <?php } ?>
                <!-- admin -->
				<li class="volver"><a href="#/"><i class="ii-reply icon23"></i><br/>Volver</a></li>
			</ul>
		</div>


		<!-- Big Menu -->

		<div class="medium-4 small-6 column small-centered" ng-hide="isSmall()">
			<br/>
			<br/>
			<div class="logo">
				<object type="image/svg+xml" data="<?php echo URL::to('/svg_sprites/logo_knowit.svg'); ?>">
					<img src="<?php echo URL::to('/svg_sprites/logo_knowit.png'); ?>" alt=""/>
				</object>
			</div>
			<br/>
			<br/>
		</div>

		<ul class="big-menu medium-block-grid-4 small-block-grid-2" ng-hide="isSmall()">
			<li class="">
				<a href="#/resumen" class="resumen">
					Resumen del día <br/>
					<img src="{{ asset('img/icon_resumen.png') }}" alt="Administrador"/>
				</a>
			</li>
			<li class="">
				<a href="#/administrador" class="administrador">
					Administrador <br/>
					<img src="{{ asset('img/icon_administrador.png') }}" alt="Administrador"/>
				</a>
			</li>
			<li class="">
				<a href="#/reportes" class="reportes">
					Reportes <br/>
					<img src="{{ asset('img/icon_reportes.png') }}" alt="Administrador"/>
				</a>
			</li>
			<li class="">
				<a href="#/base-datos" class="base-datos">
					Base de datos <br/>
					<img src="{{ asset('img/icon_base_datos.png') }}" alt="Administrador"/>
				</a>
			</li>
		</ul>
	</div>

	<!-- End popup -->

	<div id="content" ng-view=""></div>
	<div id="footer">
		<p class="text-center"><a href="http://knowit.pe">Knowit.pe</a> - Todos los derechos reservados | <a href="http://knowit.pe">Contactanos</a> | <a href="http://knowit.pe">Condiciones de servicio</a></p>
	</div>

	<script src="{{ asset('js/libs/jquery-1.10.2.js') }}"></script>

	<script src="{{ asset('packages/tweenmax/TweenMax.min.js') }}"></script>
	<script src="{{ asset('packages/tweenmax/utils/Draggable.min.js') }}"></script>
	<script src="{{ asset('packages/tweenmax/utils/Draggable.min.js') }}"></script>

	<script src="{{ asset('js/libs/angular.min.js') }}"></script>
	<script src="{{ asset('js/libs/angular-route.min.js') }}"></script>
	<script src="{{ asset('js/libs/angular-resource.min.js') }}"></script>
	<script src="{{ asset('js/libs/angular-animate.min.js') }}"></script>
	<script src="{{ asset('js/libs/mm-foundation-tpls-0.5.1.min.js') }}"></script>
	<script src="{{ asset('js/libs/jquery.datetimepicker.js') }}"></script>

	<script src="{{ asset('packages/loading_bar/loading-bar.js') }}"></script>
	<script src="{{ asset('packages/tree/angular-ui-tree.min.js') }}"></script>
	<script src="{{ asset('packages/select/angular-multi-select.js') }}"></script>
	<script src="{{ asset('packages/pickadate/angular-pickadate.js') }}"></script>
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="{{ asset('js/controllers.js') }}"></script>
	<script src="{{ asset('js/services.js') }}"></script>
	<!-- start plugin chart -->
	<script  src="{{ asset('js/libs/chartjs/knockout-3.0.0.js') }}"></script>
    <script  src="{{ asset('js/libs/chartjs/globalize.min.js') }}"></script>
    <script  src="{{ asset('js/libs/chartjs/dx.chartjs.js') }}"></script>
    <script  src="{{ asset('js/main.js')}}"></script>

    <!-- end plugin chart -->
	<script type="text/javascript">
		var appConfig = new Object();
		appConfig.root = '<?php echo URL::to('/'); ?>';
		appConfig.apiURL = '<?php echo URL::to('api/v1'); ?>';
		appConfig.views = '<?php echo URL::to('/'); ?>';
	</script>
</body>
</html>