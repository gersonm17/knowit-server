<div class="row administrador" ng-controller="KnowitController">
	<div class="container">

		<!-- Enterprises -->

		<div class="medium-3 column">
			<div class="block-content content">
				<h3 class="title"> Empresas
                    <div class="controls"><a tooltip="crear empresa" ng-click="toggleAddEnterprise()"><i class="ii-circle-plus"></i></a> </div>
                </h3>

				<div class="list">
                    <!-- Start form enterprise -->

                    <div class="form-white hide-anim" ng-show="addEnterpriseOpen">

                        <p>Nombre: <br/> <input type="text" ng-model="currentEnterpriseEditCopy.name"/></p>
                        <p>Descripcion: <br/> <input type="text" ng-model="currentEnterpriseEditCopy.description"/></p>
						<p>Usuario: <br/> <input type="text" ng-model="currentEnterpriseEditCopy.user"/></p>
						<p>E-mail: <br/> <input type="text" ng-model="currentEnterpriseEditCopy.email"/></p>
						<p>Contraseña: <br/> <input type="text" ng-model="currentEnterpriseEditCopy.password"/></p>
                        <br/>
                        <p class="text-center">
                            <a class="button tiny alert" ng-click="closeAddEnterprise()">Cancelar</a>
                            <a class="button tiny success" ng-click="saveEnterprise()">Guardar</a>
                        </p>
                    </div>

                    <!-- End form enterprise -->
                    <ul class="list-menu">
                        <li ng-repeat="d in companies.data">
                            <div class="controls">
                                <a ng-click="editEnterprise(d)"><i class="ii-pencil3 icon16"></i></a>
                                <a ng-click="deleteEnterprise(d)"><i class="ii-trash icon16"></i></a>
                            </div>
                            <a ng-click="selectEnterprise($event, d)">
                                {{ d.name }}
                                <span>{{d.description}}</span>
                            </a>
                        </li>
                    </ul>
					<!--<accordion close-others="false">
						<accordion-group ng-repeat="d in companies.data" is-open="true">
							<accordion-heading><i class="{{(d.type=='web')?'ii-earth2':'ii-folder'}} icon16 right text-right"></i> {{ d.name }}</accordion-heading>
                            <span></span>
							<p ng-if="d.segments.length == 0" class="msg msg-info"><i class="ii-info"></i> No hay empresas asignadas</p>
							<ul class="list-editable list-small">
                                <li>{{d.description}}
                                    <a class="right " tooltip="Quitar de esta empresa" ng-click="removeFromEnterprise(d)"><i class="ii-circle-cross icon16"></i></a>
                                </li>
							</ul>
						</accordion-group>
					</accordion>-->
				</div>
			</div>
		</div>

		<!-- places -->

		<div class="medium-3 column">
			<div class="block-content content">
				<h3 class="title">
					Dispositivos
					<!--<div class="controls"><a tooltip="Crear lugar" ng-click="toggleAddPlace()"><i class="ii-circle-plus"></i></a> </div>-->
				</h3>
				<div class="list">

					<!-- Start form places -->

					<div class="form-white hide-anim" ng-show="addDeviceOpen">

						<p>Nombre: <br/> <input type="text" ng-model="currentDeviceEditCopy.name"/></p>
						<!--<p>uid: <br/> <input type="text" ng-model="currentDeviceEditCopy.uid"/></p>
						<p>Alias: <br/> <input type="text" ng-model="currentDeviceEditCopy.alias"/></p>-->

                        <span>Empresa:</span>
                        <div multi-select input-model="companies.data" output-model="currentEnterpriseEditCopy" helper-elements="" default-label="Seleccionar empresas"
                             button-label="name" item-label="name"  tick-property="selected" max-labels="1" selection-mode="single">
                        </div>


						<!--<div multi-select input-model="devicesActive" output-model="currentEnterpriseEditCopy" helper-elements="" default-label="{{(currentDeviceEditCopy.disabled=='0'?'Activo':'Inactivo')}}"
							 button-label="name" item-label="name"  tick-property="selected" max-labels="1" selection-mode="single">
						</div>-->


						<p class="text-center">
							<a class="button tiny alert" ng-click="closeAddDevice()">Cancelar</a>
							<a class="button tiny success" ng-click="saveDevice()">Guardar</a>
						</p>
					</div>

					<!-- End form places -->

					<div class="tip" ng-show="devices.data.length == 0"><h3 class="tip-title">No tienes dispositivos </h3><p>
							</p></div>

					<ul class="list-menu">

						<li ng-repeat="d in devices.data" >
							<div class="controls">
								<a ng-click="editDevice(d)" ><i class="ii-pencil3 icon16"></i></a>
								<!--<a ng-click="deleteDevice(d)" tooltip="Eliminar"><i class="ii-trash"></i></a>-->
							</div>
							<a ng-click="selectDevice($event, d)">{{ d.name }}
                                <span>{{d.name}}</span>
                            </a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- segmets -->

		<div class="medium-6 columns">
			<!--<div class="block-list">
				<h3 class="title">

					<div class="controls"><a tooltip="Editar encuestas" ng-click="editSurveys()"><i class="ii-paper"></i></a> </div>
				</h3>
				<div class="list">
					<p ng-show="currentSegment != null">Encuesta:
						<select ng-model="selectSurveyID" class="large-4" ng-change="changeSurvey()">
							<option value="-1">Sin encuesta</option>
							<option ng-repeat="sv in surveys.data" value="{{sv.id}}">{{sv.name}}</option>
						</select>
					</p>

					<div class="tip" ng-show="(currentSurvey.data == null || currentSurvey.data.questions.length == 0) && currentSegment != null">
						<h3 class="tip-title">No hay preguntas en este segmento</h3>
						<p>Puedes elegir una encuesta distinta o bien <a ng-click="editSurveys()">gestionar tus preguntas aquí</a></p>
					</div>

					<div class="msg-info" ng-hide="currentSegment != null"><i class="ii-arrow-left2"></i> </div>

					<ul class="list-editable">
						<li ng-repeat="q in currentSurvey.data.questions" class="list-anim">
							<span class="right label">{{q.type}}</span>
							{{ q.question }}
						</li>
					</ul>
				</div>
			</div>-->
		</div><!-- End preguntas -->
	</div><!-- End container -->
</div>