<script>
    var objHelp={
        setting:"",
        convertDate: function(inputFormat){
            function pad(s) { return (s < 10) ? '0' + s : s; }
            var d = new Date(inputFormat);
            return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
        }
    };
</script>
<div class="base-datos row" ng-controller="BaseDatosController">
	<div class="container">

		<!-- Primera columna -->

		<div class="medium-3 columns">
			<div class="block-list" ng-repeat="place in places.data">
				<h3 class="title">
					<div class="controls">
						<a ng-click="selectAll(place)" tooltip="Seleccionar todos"><i class="ii-circle-check icon19"></i></a>
						<a ng-click="selectNone(place)" class="icon-anim" ng-show="place.hasSelected" tooltip="Deseleccionar todo"><i class="ii-circle-cross icon19"></i></a>
					</div>
					<i class="ii-directions"></i> {{place.name}}
				</h3>

				<!-- Lista dispositivos -->
				<div class="list">
					<div class="msg msg-info" ng-show="place.devices.length == 0">No tienes dispositivos en este lugar, para agregarlos ingresa <a href="#/administrador">aquí</a></div>
					<div ng-repeat="device in place.devices">
						<div class="sub-title" tooltip="{{device.name}}">{{device.alias || device.name }}</div>
						<ul class="list-editable">
							<li ng-repeat="s in device.segments" class="list-anim">
								<span tooltip="{{ s.description }}">{{ s.name }}</span>
								<fieldset class="switch round tiny right">
									<input id="segment_{{device.id}}_{{s.id}}" type="checkbox" ng-model="s.selected" ng-change="updateDetail()">
									<label for="segment_{{device.id}}_{{s.id}}"></label>
								</fieldset>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Sección de contenido -->

		<div class="medium-9 columns">
			<div class="block-content content">
				<div class="right">
					<div class="block-details">
						<div class="block-details-item">
							<i class="ii-users3 icon32" style="margin-left: 5px;"></i>
						</div>
						<div class="block-details-item icon23">
							= {{ customers.pagination.total || '?' }}
						</div>
						<div class="block-details-item">
							<i class="ii-arrow-right2"></i>
						</div>
						<div class="block-details-item">
							<i class="ii-calendar icon32"></i>
						</div>
						<div class="block-details-item">
                            Desde: <a></a> <a ng-model="start_time_bd" id="start_timebd"><input id="start_timebd"  style="display: none" type="text" > </a> <a class="label round success">06:30</a> <br/>
                            Hasta: <a></a> <a ng-model="end_time_bd" id="end_timebd" ><input id="end_timebd" style="display: none" type="text" > </a> <a class="label round success">22:00</a>
						</div>
					</div>
					<!--<div class="datepicker content">
						<div pickadate ng-model="dateStart"></div>
					</div>-->
				</div>

				<h3>Base de datos de registros y respuestas</h3>
				<br/>

				<div class="tip" ng-hide="segmentsSelected.length > 0">
					<h3 class="tip-title"><i class="ii-info icon32 right"></i> Elige un segmento</h3>
					<p>Selecciona uno o más segmentos para ver las respuestas de las personas.</p>
				</div>

				<table class="small-12" ng-show="customers.data.length > 0">
					<thead>
					<tr>
						<th >Nombre</th>
						<th >E-mail</th>
						<th>Comentario</th>
						<th>Segmento</th>
						<th>Respuestas</th>
						<th width="120">Fecha de registro</th>
					</tr>
					</thead>
					<tbody>
						<tr ng-repeat="c in customers.data">
							<td>{{ c.name }}</td>
							<td>{{ c.email }}</td>
							<td>{{ c.comments }}</td>
							<td>{{ c.segment_name }}</td>
							<td style="vertical-align: middle;">
								<span ng-repeat="answer in c.answers" tooltip="{{answer.question.question}}">
									<span ng-switch on="answer.question.type">
										<span ng-switch-when="default">
											<span ng-switch on="answer.raw_answer">
												<i ng-switch-when="0" class="ii-knowit_faces_1 icon16"></i>
												<i ng-switch-when="1" class="ii-knowit_faces_2 icon16"></i>
												<i ng-switch-when="2" class="ii-knowit_faces_3 icon16"></i>
												<i ng-switch-when="3" class="ii-knowit_faces_4 icon16"></i>
											</span>
										</span>
										<span ng-switch-when="boolean">
											<span ng-switch on="answer.raw_answer">
												<span ng-switch-when="si" class="label round">SI</span>
												<span ng-switch-when="no" class="label round">NO</span>
											</span>
										</span>
										<span ng-switch-when="custom">
											<span tooltip="{{answer.raw_answer}}" tooltip-placement="bottom">
												<i class="ii-info icon16"></i>
											</span>
										</span>
										<span ng-switch-when="options">
											<span ng-switch on="answer.raw_answer">
												<span ng-switch-when="si" class="label round">SI</span>
												<span ng-switch-when="no" class="label round">NO</span>
											</span>
										</span>
									</span>
								</span>
							</td>
							<td>{{ c.register_at | date : 'dd/yyyy'}}</td>
						</tr>
					</tbody>
				</table>

				<pagination ng-show="customers.data.length > 0" total-items="customers.pagination.total"
							page="customers.pagination.current_page"
							previous-text="Anterior" next-text="Siguiente"
							on-select-page="updateDetail(page)"
							items-per-page="customers.pagination.per_page"></pagination>
			</div>
            <div  ng-show="customers.data.length > 0" style="display: table;clear: both;padding-top: 36px;">
                <hr>
                <p class="text-center btn-export">
                    <a id="excel" target="_blank"   class="btn btn-pdf" >Descargar en formato excel</a>
                </p>
            </div>
        </div>
	</div>
</div>
<script>
    $(document).ready(initPage);
    function initPage(){
        var f_start = new Date('2015-2-1');
        var f_end   = new Date();
        $('#start_timebd').val(objHelp.convertDate(f_start));
        $('#end_timebd').val(objHelp.convertDate(f_end));
        //console.log('set dates');
    }
    $('.btn-export a').click(CLICK_handler);
    function CLICK_handler(){
        if(objHelp.segselec.length>0){
            var  segments= encodeURIComponent(JSON.stringify(objHelp.segselec));
            
            var fechas = '?selected=' + segments + '&since=' + objHelp.since + '&until=' + objHelp.until;
            switch ($(this).attr('id'))
            {
                case 'excel':
                    window.open('<?php echo URL::to('api/v1/report/excel'); ?>' + fechas,'_blank');
                    break;
            }
        }

    } /**/
</script>
