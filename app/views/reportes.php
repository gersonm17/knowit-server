<div class="reportes row" ng-controller="ReportesController">

	<div class="container">

		<!-- Primera columna -->

		<div class="medium-3 columns">
			<div class="block-list" ng-repeat="place in places.data">
				<h3 class="title">
					<div class="controls">
						<a ng-click="selectAll(place)" tooltip="Seleccionar todos"><i
								class="ii-circle-check icon19"></i></a>
						<a ng-click="selectNone(place)" class="icon-anim" ng-show="place.hasSelected"
						   tooltip="Deseleccionar todo"><i class="ii-circle-cross icon19"></i></a>
					</div>
					<i class="ii-directions"></i> {{place.name}}
				</h3>

				<!-- Lista dispositivos -->
				<div class="list">
					<div class="msg msg-info" ng-show="place.devices.length == 0">No tienes dispositivos en este lugar,
						para agregarlos ingresa <a href="#/administrador">aquí</a></div>
					<div ng-repeat="device in place.devices">
						<div class="sub-title" tooltip="{{device.name}}">{{device.alias || device.name }}</div>
						<ul class="list-editable">
							<li ng-repeat="s in device.segments" class="list-anim">
								<span tooltip="{{ s.description }}">{{ s.name }}</span>
								<fieldset class="switch round tiny right">
									<input id="segment_{{device.id}}_{{s.id}}" type="checkbox" ng-model="s.selected"
										   ng-change="updateDetail()">
									<label for="segment_{{device.id}}_{{s.id}}"></label>
								</fieldset>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Sección de contenido -->

		<div class="medium-9 columns">
			<div class="block-content content">

				<div class="right">
					<div class="block-details">
						<div class="block-details-item">
							<i class="ii-users3 icon32" style="margin-left: 5px;"></i>
						</div>
						<div class="block-details-item icon23">
							= {{ customers || '?' }}
						</div>
						<div class="block-details-item">
							<i class="ii-arrow-right2"></i>
						</div>
						<div class="block-details-item">
							<i class="ii-calendar icon32"></i>
						</div>
						<div class="block-details-item">
							Desde: <a></a><a ng-model="start_time_" id="start_time"> <input id="start_time"
																							style="display: none"
																							type="text"> </a> <a
								class="label round success">06:30</a> <br/>
							Hasta: <a></a> <a ng-model="end_time_" id="end_time"> <input id="end_time"
																						 style="display: none"
																						 type="text"> </a> <a
								class="label round success">22:00</a>
						</div>
					</div>
					<!--<div class="datepicker content">
						<div pickadate ng-model="dateStart"></div>
					</div>-->
				</div>
				<!-- Selección rango de fechas -->
				<h2>Detalle de segmentos seleccionados</h2>
				<br/>

				<div class="tip" ng-hide="segmentsSelected.length > 0">
					<h3 class="tip-title">Elige un segmento</h3>

					<p>Puedes seleccionar uno o más segmentos, luego podrás ver el detalle aquí.</p>
				</div>

				<div ng-show="segmentsSelected.length > 0">
					<!-- contenedorde gráfico -->
					<div id="promedio_aprobacion" style="width: 100%;height: 230px;"></div>

					<hr/>
					<!--- to start count -->
					<h4>Conteo de totales</h4>

					<div>
						<div class="small-9 small-centered columns">
							<div class="medium-3 columns">
								<div class="circle-m">
									<span class="total-preguntas numbercircle"></span>
									<br>Preguntas mostradas
								</div>
							</div>
							<div class="medium-3 columns">
								<div class="circle-m">
									<span class="total-opiniones numbercircle"></span>
									<br>Respuestas recibidas
								</div>
							</div>
							<div class="medium-3 columns">
								<div class="circle-m">
									<span class="peor-promedio numbercircle"></span>
									<br>Total de encuestas
								</div>
							</div>
							<div class="medium-3 columns end">
								<div class="circle-g">
									<span class="mejor-promedio numbercircle"></span>
									<br>Porcentaje de aprobación
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<!--- end count -->
					<hr/>

					<!--- start circle chart-->
					<p class="right"><img src="<?php echo URL::asset('img/face4.jpg'); ?>" alt="Caras" width="300"/></p>
					<h4>Conteo por pregunta</h4>

					<div class="report-list-question"></div>

					<!--- start circle chart-->
					<hr>
					<p class="text-center">
						<a onclick="DownloadPdf()" id="pdf"  class="button large"><i class="ii-file-pdf"></i> Descargar reporte</a>
							<!--<button ng-click="getPdf()" target="_blank" class="btn" ng-init="count=0">Descargar en formato pdf</button>
							<a href="#excel" target="_blank" class="btn">Descargar en formato excel</a>-->
					</p>
				</div>
				<!-- End report container -->
			</div>
		</div>
		<hr/>
		<script>
			$(document).ready(initPage);

			var f_start = '<?php echo new Carbon('last monday'); ?>';
			var res = f_start.split(" ");
			var f_end = '<?php echo Carbon::now()->format('Y-m-d')?>';
			var selectedRange = {};
			f_start = res[0];

			function initPage() {
				$('#start_time').val(f_start);
				$('#end_time').val(f_end);
			}

			$('#pdf').click(function(){
				if (selectedRange.segmentsSelected.length > 0) {
					var segments = encodeURIComponent(JSON.stringify(selectedRange.segmentsSelected));
					var fechas = '?segments=' + segments + '&start_date=' + $('#start_time').val() + '&end_date=' + $('#end_time').val();
					window.open('<?php echo URL::to('reportPdf/pdf-date'); ?>' + fechas, '_blank');


				}
			});

		</script>



	</div>
</div>


