<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="es"><!--<![endif]-->
<head>
    <meta charset=utf-8 />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Formax &#8211; Estrategias de modelos de negocio online">
    <meta name="description" content="o">
    <title>FeedBox &#8211; Conoce la opinión de tus clientes</title>

    <script type="text/javascript" src="<?php echo URL::to('js/libs/jquery-1.10.2.js'); ?>"></script>

    <!--[if lt IE 9]><script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>	<![endif]-->
    <link type="text/css" href="<?php echo URL::to('css/style.css'); ?>" rel="stylesheet" />
    <link type="text/css" href="<?php echo URL::to('css/sprites.css'); ?>" rel="stylesheet" />

    <link rel="shortcut icon" href="<?php echo URL::to('favicon.png'); ?>">
</head>
<div class="content">

    <div id="<?php echo $segment_id ?>" class="row">
        <ul id="list-questions" class="list-items " >
            <?php
            foreach($questions as $q)
            {
                echo '<li class="q_'.$q->id.'" id="'.$q->id.'">' . $q->question . '</li>';
            }?>
        </ul>

    </div>
    <div class="row smile-button">
        <div class="col-lg-3"><a href="#sad"><span class="face-angry face-emotion"></span> Sad</a></div>
        <div class="col-lg-3"><a href="#neutral"><span class="face-sad face-emotion"></span> Neutral</a></div>
        <div class="col-lg-3"><a href="#smiley"><span class="face-smiley face-emotion"></span> Smiley</a></div>
        <div class="col-lg-3"><a href="#happy"><span class="face-happy face-emotion"></span> Happy</a></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(initPage);

    var current_question = 0;
    var params = {
        question_id:-1,
        code:'9c0e2407f4022c2fA3',
        answer:'',
        segment_id:''
    };

    function initPage()
    {
        $('.smile-button a').click(BTN_handler);
        var q = $('#list-questions li:eq('+current_question+')');
        q.css('font-weight', 'bold');
        q.addClass('font-weight', 'bold');
        params.question_id = q.attr('id');
    }

    function BTN_handler()
    {
        fech = new Date();
        var day = fech.getDate();       //
        var month = fech.getMonth()+1;    //
        var year = fech.getFullYear();
        var hours = fech.getHours();
        var minutes = fech.getMinutes();
        var seconds = fech.getSeconds();
        params.answer_at=year+'-'+month+'-'+day+' '+ hours+':'+minutes+':'+seconds;
        switch ($(this).attr('href'))
        {
            case '#sad': params.answer = 1; break;
            case '#neutral': params.answer = 2; break;
            case '#smiley': params.answer = 3; break;
            case '#happy': params.answer = 4; break;
        }

        sendForm();
    }

    function sendForm()
    {

        params.segment_id=$('.row').attr('id');
        console.log(params);

        $.post('<?php echo URL::to('questions/save'); ?>', params, function(){
            $('.question_list li:eq('+current_question+')').css('font-weight', 'normal');
            current_question++;
            if(current_question >= $('.question_list li').length) current_question = 0;
            $('.question_list li:eq('+current_question+')').css('font-weight', 'bold');
            $('.question_list li:eq('+current_question+')').css('font-weight', 'bold');
            params.question_id = $('.question_list li:eq('+current_question+')').attr('id');
        });
        /**/
    }
</script>