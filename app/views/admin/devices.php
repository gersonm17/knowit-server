<div class="row administrador">
	<div class="container">

		<!-- Dispositivos -->

		<div class="medium-3 column">
			<div class="block-list">
				<h3 class="title"> Dispositivos asignados</h3>
				<div class="list">
					<ul class="list-editable">
						<?php foreach($devices as $d)
						{
							echo '<li>';
							if($d->company)
								echo '<div class="controls">' . $d->company->name . '</div>';
							else
								echo '<div class="controls"><a href="'. URL::to('devices/add-device/' . $d->id) .'">Asignar</a></div>';
							echo $d->description . '<br/> <span>' . $d->uid . '</span></li>';
						} ?>
					</ul>
				</div>
			</div>
		</div>

	</div>
</div>