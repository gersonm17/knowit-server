<div class="survey" >
    <a class="right" ng-click="cancel()"><i class="ii-cross icon23"></i></a>
    <h3>Lugares/Dispositivos</h3>

    <div class="container">

        <!-- Primera columna -->

        <div class="medium-4 columns">
            <h4>
                <div class="controls">
                    <a tooltip="" ng-click="toggleNewPlace()"><i class="ii-circle-plus"></i></a>
                </div>
                Lugares
            </h4>

            <div class="form-white" ng-show="isPlaceOpen">
                <p> Nombre: <br/> <input type="text" ng-model="placeEditCopy.name"/></p>
                <p> Descripcion: <br/> <input type="text" ng-model="placeEditCopy.description"/></p>

                <p>
                    <button class="button" ng-click="togglePlace()">Cancelar</button>
                    <button class="button" ng-click="savePlace()">Guardar</button>
                </p>
            </div>

            <ul class="list-menu">
                <li ng-repeat="s in places.data">
                    <div class="right">

                        <a ng-click="editPlace(s)"><i class="ii-pencil3 icon16"></i></a>
                        <a ng-click="deletePlace(s)"><i class="ii-trash icon16"></i></a>
                    </div>
                    <a ng-click="selectPlace($event, s)">
                        {{ s.name }}
                        <span>{{s.description}}</span>
                    </a>
                </li>
            </ul>

        </div>

        <!-- Secci�n de contenido -->

        <div class="medium-8 columns">
            <h4>
                <div class="controls" ng-show="currentPlace != null">
                    <a ng-click="toggleNewDevice()"><i class="ii-circle-plus"></i></a>
                </div>
                Dispositivos
            </h4>

            <div class="form-white" ng-show="isDeviceOpen">
                <br>
                <div class="row">
                    <div class="medium-11 medium-centered columns">
                        <div multi-select input-model="devices.data" output-model="deviceEditCopy.data" helper-elements="" default-label="Seleccionar dispositivo"
                             button-label="name" item-label="name"  tick-property="selected" max-labels="1" selection-mode="single">
                        </div>
                    </div>
                </div>

                <br>
                <p class="text-center">
                    <button class="button" ng-click="toggleDevice()">Cerrar</button>
                    <button class="button" ng-click="saveDevice()">Guardar</button>
                </p>
            </div>

            <div class="msg msg-info" ng-show="currentPlace == null">Elige un lugar para ver los dispositivos</div>
            <div class="tip" ng-show="currentPlace != null && currentPlace.devices.length == 0">
                <h3 class="tip-title">Este lugar no tiene dispositivos</h3>
                <p>Haz clic en el mas <i class="ii-circle-plus"></i> para crear nuevos dispositivos</p>
            </div>

            <ul class="list-items">
                <li ng-repeat="d in currentPlace.devices" class="list-anim">
                    <div class="controls">
                        <a ng-click="editDevice(d)"><i class="ii-pencil3"></i></a>
                        <a ng-click="deleteDevice(d)"><i class="ii-trash"></i></a>
                    </div>
                    {{ d.name }}
                    <span style="font-size: .8rem;color: #afafaf;" >{{d.alias}}</span>
                </li>
            </ul>
        </div>
    </div>
</div>