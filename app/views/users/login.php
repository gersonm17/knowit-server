<form method="POST" action="http://localhost/knowitv2/users/login" accept-charset="UTF-8">
	<input type="hidden" name="_token" value="opsahkuWiyLUIiuh72qPpoNa0dVo80i2szhTwyi1">
	<fieldset>
		<div class="form-group">
			<label for="email">Username or Email</label>
			<input class="form-control" tabindex="1" placeholder="Username or Email" type="text" name="email" id="email"
				   value="">
		</div>
		<div class="form-group">
			<label for="password">
				Password
				<small>
					<a href="http://localhost/knowitv2/users/forgot_password">(forgot password)</a>
				</small>
			</label>
			<input class="form-control" tabindex="2" placeholder="Password" type="password" name="password"
				   id="password">
		</div>
		<div class="form-group">
			<label for="remember" class="checkbox">Remember me
				<input type="hidden" name="remember" value="0">
				<input tabindex="4" type="checkbox" name="remember" id="remember" value="1">
			</label>
		</div>

		<div class="form-group">
			<button tabindex="3" type="submit" class="btn btn-default">Login</button>
		</div>
	</fieldset>
</form>