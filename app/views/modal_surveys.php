<div class="survey" >
	<a class="right" ng-click="cancel()"><i class="ii-cross icon23"></i></a>
	<h3>Gestionar encuestas</h3>

	<div class="container">

		<!-- Primera columna -->

		<div class="medium-4 columns">
			<h4>
				<div class="controls">
					<a tooltip="" ng-click="toggleSurvey()"><i class="ii-circle-plus"></i></a>
				</div>
				Encuestas
			</h4>

			<div class="form-white" ng-show="isSurveyOpen">
				<p> Nombre: <br/> <input type="text" ng-model="surveyEditCopy.name"/></p>
				<p> Descripción: <br/> <input type="text" ng-model="surveyEditCopy.description"/></p>
				<p>
					<button class="button" ng-click="toggleSurvey()">Cancelar</button>
					<button class="button" ng-click="saveSurvey()">Guardar</button>
				</p>
			</div>

			<ul class="list-menu">
				<li ng-repeat="s in surveys.data">
					<div class="controls">
						<a ng-click="editSurvey(s)"><i class="ii-pencil3 icon16"></i></a>
						<a ng-click="deleteSurvey(s)"><i class="ii-trash icon16"></i></a>
					</div>
					<a ng-click="selectSurvey($event, s)">
						{{ s.name }}
						<span>{{s.description}}</span>
					</a>
				</li>
			</ul>

		</div>

		<!-- Sección de contenido -->

		<div class="medium-8 columns">
			<h4>
				<div class="controls" ng-show="currentSurvey != null">
					<a ng-click="toggleQuestion()"><i class="ii-circle-plus"></i></a>
				</div>
				Preguntas
			</h4>

			<div class="form-white" ng-show="isQuestionOpen">
				<p> Pregunta: <br/> <input type="text" ng-model="questionEditCopy.question"/></p>
				<div class="container">
					<div class="medium-4 column">
						<p> Tipo: <br/>
							<select name="type" ng-model="questionEditCopy.type">
								<option value="default">Calidad</option>
								<option value="boolean">Si & No</option>
								<option value="custom">Respuesta escrita</option>
								<option value="options">Opciones</option>
							</select>
						</p>
					</div>
					<div class="medium-8 column">
						<p ng-show="questionEditCopy.type == 'options'"> Opciones: <br/> <input type="text" ng-model="questionEditCopy.options"/></p>
					</div>
				</div>
				<p class="clearfix">
					<fieldset class="switch round tiny left">
						<input id="segment_has_comment" type="checkbox" ng-model="questionEditCopy.has_comment" ng-true-value="1">
						<label for="segment_has_comment"></label>
					</fieldset>
					<label for="segment_has_comment"> Habilitar comentarios</label>
				</p>
				<p class="text-center">
					<button class="button" ng-click="toggleQuestion()">Cerrar</button>
					<button class="button" ng-click="saveQuestion()">Guardar</button></p>
			</div>

			<div class="msg msg-info" ng-show="currentSurvey == null">Elige una encuesta para ver las preguntas</div>
			<div class="tip" ng-show="currentSurvey != null && currentSurvey.questions.length == 0">
				<h3 class="tip-title">Esta encuesta no tiene preguntas</h3>
				<p>Haz clic en el más <i class="ii-circle-plus"></i> para crear nuevas preguntas</p>
			</div>

			<ul class="list-items">
				<li ng-repeat="q in currentSurvey.questions" class="list-anim">
					<div class="controls">
						<a ng-click="editQuestion(q)"><i class="ii-pencil3"></i></a>
						<a ng-click="deleteQuestion(q)"><i class="ii-trash"></i></a>
					</div>
					<span class="label" ng-switch on="q.type">
						<span ng-switch-when="default"><i class="ii-price-tag"></i> Calidad</span>
						<span ng-switch-when="boolean"><i class="ii-price-tag"></i> Si / No</span>
						<span ng-switch-when="custom"><i class="ii-price-tag"></i> Respuesta</span>
						<span ng-switch-when="options"><i class="ii-price-tag"></i> Opciones</span>
					</span>
					{{ q.question }}
				</li>
			</ul>
		</div>
	</div>
</div>
