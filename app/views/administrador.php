<div class="row administrador" ng-controller="AdministradorController">
	<div class="container">

		<!-- Dispositivos -->

		<div class="medium-3 column">
			<div class="block-content">
				<div class="content">
					<h4 class="title"> Lugares / Dispositivos</h4>
					<div class="text-center">
						<a class="button" ng-click="editPlace()"><i class="ii-map"></i> Lugares / Dispositivos</a>
					</div>
				</div>
				<div class="list">
					<accordion close-others="false">
						<accordion-group ng-repeat="d in devices.data" is-open="true">
							<accordion-heading><span class="right"><i class="{{(d.type=='web')?'ii-earth2':'ii-mobile2'}} icon16"></i></span> {{ d.name }}</accordion-heading>
							<div ng-if="d.segments.length == 0" class="msg msg-info"><i class="ii-info"></i> No hay segmentos asignados</div>
							<ul class="list-editable">
								<li ng-repeat="s in d.segments">
									<a class="right" tooltip="Quitar de este dispositivo" ng-click="removeFromDevice(s, d)"><i class="ii-circle-cross icon16"></i></a>
									<span tooltip="{{ s.description }}">{{ s.name }}</span>
								</li>
							</ul>
						</accordion-group>
					</accordion>
				</div>
			</div>
		</div>

		<!-- Segmentos -->

		<div class="medium-3 column">
			<div class="block-list">
				<h3 class="title">
					Segmentos
					<div class="controls"><a tooltip="Crear segmento" ng-click="toggleAddSegment()"><i class="ii-circle-plus"></i></a> </div>
				</h3>
				<div class="list">

					<!-- Start form segments -->

					<div class="form-inset hide-anim" ng-show="addSegmentOpen">
						<p>Nombre: <br/> <input type="text" ng-model="currentSegmentEditCopy.name"/></p>
						<p>Detalles del segmento: <br/> <input type="text" ng-model="currentSegmentEditCopy.description"/></p>

						<p><span class="has-tip" tooltip="Al llenar este campo se convertirá en seguro y se tendrá que ingresar la contraseña para poder usar este segmento">Contraseña:</span> <br/> <input type="text" ng-model="currentSegmentEditCopy.password"/></p>
						<p><input id="hasform" type="checkbox" ng-model="currentSegmentEditCopy.has_form"/> <label style="color: inherit" for="hasform">Mostrar formulario</label> </p>
						<span>Dispositivos donde se mostrará:</span>
						<div multi-select input-model="devices.data" output-model="currentSegmentEditCopy.devices" helper-elements="" default-label="Seleccionar dispositivos"
							button-label="name" item-label="name" tick-property="selected" max-labels="1">
						</div>
						<br/>
						<p class="text-center">
							<a class="button alert" ng-click="closeAddSegment()">Cancelar</a>
							<a class="button success" ng-click="saveSegment()">Guardar</a>
						</p>
					</div>

					<!-- End form segments -->

					<div class="tip" ng-show="segments.data.length == 0"><h3 class="tip-title">No tienes segmentos creados</h3><p>Puedes crear segmentos nuevos haciendo click
							<a ng-click="toggleAddSegment()">aquí</a></p></div>

					<ul class="list-editable">
						<li ng-repeat="s in segments.data" class="list-anim">
							<div class="controls">
								<a ng-click="editSegment(s)" tooltip="Editar"><i class="ii-pencil3"></i></a>
								<a ng-click="deleteSegment(s)" tooltip="Eliminar"><i class="ii-trash"></i></a>
							</div>
							<a ng-click="selectSegment($event, s)" tooltip="{{ s.description }}"><i class="{{(s.password.length > 0)?'ii-lock':'ii-unlocked'}}"></i> {{ s.name }}</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Preguntas -->

		<div class="medium-6 columns">
			<div class="block-list">
				<h3 class="title">
					Preguntas
					<div class="controls">
						<a tooltip="Editar encuestas" class="button small" ng-click="editSurveys()">Editar encuestas <i class="ii-paper"></i></a></div>
				</h3>
				<div class="list">
					<p ng-show="currentSegment != null">Encuesta:
						<select ng-model="selectSurveyID" class="large-4" ng-change="changeSurvey()">
							<option value="-1">Sin encuesta</option>
							<option ng-repeat="sv in surveys.data" value="{{sv.id}}">{{sv.name}}</option>
						</select>
					</p>

					<div class="tip" ng-show="(currentSurvey.data == null || currentSurvey.data.questions.length == 0) && currentSegment != null">
						<h3 class="tip-title">No hay preguntas en este segmento</h3>
						<p>Puedes elegir una encuesta distinta o bien <a ng-click="editSurveys()">gestionar tus preguntas aquí</a></p>
					</div>

					<div class="msg msg-info" ng-hide="currentSegment != null"><i class="ii-arrow-left2"></i> Elige un segmento</div>

					<ul class="list-editable">
						<li ng-repeat="q in currentSurvey.data.questions" class="list-anim">
							<span class="right" ng-switch on="q.type">
								<span class="label" ng-switch-when="default"><i class="ii-price-tag"></i> Calidad</span>
								<span class="label" ng-switch-when="boolean"><i class="ii-price-tag"></i> Si / No</span>
								<span class="label" ng-switch-when="custom"><i class="ii-price-tag"></i> Respuesta</span>
								<span class="label" ng-switch-when="options"><i class="ii-price-tag"></i> Opciones</span>
							</span>
							{{ q.question }}
						</li>
					</ul>
				</div>
			</div>
		</div> <!-- End preguntas -->
	</div><!-- End container -->
</div>