<?php
/**
 * Created by PhpStorm.
 * User: Gerson Aduviri
 * Date: 27/11/2014
 * Time: 03:55 AM
 */

class Question extends Eloquent {

	public function survey()
	{
		return $this->hasOne('Survey');
	}

}