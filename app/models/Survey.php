<?php
/**
 * Created by PhpStorm.
 * User: Gerson Aduviri
 * Date: 27/11/2014
 * Time: 03:55 AM
 */

class Survey extends Eloquent {

	public function questions()
	{
		return $this->hasMany('Question');
	}

}