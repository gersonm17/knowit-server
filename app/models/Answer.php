<?php
/**
 * Created by PhpStorm.
 * User: Gerson Aduviri
 * Date: 27/11/2014
 * Time: 03:55 AM
 */

class Answer extends Eloquent {
    public $timestamps = false;

	public function question()
	{
		return $this->belongsTo('Question');
	}

	public function customer()
	{
		return $this->belongsTo('Customer');
	}

	public function segment()
	{
		return $this->belongsTo('Segment');
	}
}