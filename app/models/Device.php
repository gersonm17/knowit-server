<?php
/**
 * Created by PhpStorm.
 * User: Gerson Aduviri
 * Date: 27/11/2014
 * Time: 03:55 AM
 */

class Device extends Eloquent {

	protected $appends = array('is_disabled');

	public function segments()
	{
		return $this->belongsToMany('Segment');
	}

	public function company()
	{
		return $this->belongsTo('Company');
	}

	public function getIsDisabledAttribute()
	{
		if($this->attributes['disabled'] == 1)
			$this->attributes['disabled'] = true;
		else
			$this->attributes['disabled'] = false;
	}

}