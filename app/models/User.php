<?php

use Zizaco\Entrust\HasRole;
use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Laravel\Cashier\BillableTrait;
use Laravel\Cashier\BillableInterface;

class User extends Eloquent implements ConfideUserInterface, BillableInterface
{
	use ConfideUser;
	use HasRole;
	use BillableTrait;

	protected $dates = ['trial_ends_at', 'subscription_ends_at'];

	public function person()
	{
		return $this->hasOne('Person');
	}

	public function companyOwner()
	{
		return $this->hasOne('Company');
	}

	public function company()
	{
		return $this->belongsTo('Company');
	}
}