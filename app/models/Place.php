<?php
/**
 * Created by Gerson Aduviri
 * Date: 10/02/2015
 * Time: 07:35 AM
 */

class Place extends Eloquent {

	public function devices()
	{
		return $this->hasMany('Device');
	}

	public function company()
	{
		return $this->belongsTo('Company');
	}


}