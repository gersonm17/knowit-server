<?php
/**
 * Created by Gerson Aduviri
 * Date: 10/02/2015
 * Time: 07:35 AM
 */

class Subscription extends Eloquent {

	public function customers()
	{
		return $this->hasMany('Customer');
	}

	public function segment()
	{
		return $this->hasOne('Segment');
	}

	public function company()
	{
		return $this->belongsTo('Company');
	}


}