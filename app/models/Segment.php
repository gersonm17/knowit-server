<?php
/**
 * Created by PhpStorm.
 * User: Gerson Aduviri
 * Date: 27/11/2014
 * Time: 03:55 AM
 */

class Segment extends Eloquent {

	public function survey()
	{
		return $this->belongsTo('Survey');
	}

	public function devices()
	{
		return $this->belongsToMany('Device');
	}

	public function company()
	{
		return $this->belongsTo('Company');
	}
}