<?php
/**
 * Created by PhpStorm.
 * User: Gerson Aduviri
 * Date: 27/11/2014
 * Time: 03:55 AM
 */

class Company extends Eloquent {

	public function companyOwner()
	{
		return $this->hasOne('User');
	}

	public function users()
	{
		return $this->hasMany('User');
	}

	public function devices()
	{
		return $this->hasMany('Device');
	}

	public function segments()
	{
		return $this->hasMany('Segment');
	}

	public function places()
	{
		return $this->hasMany('Place');
	}

	public function surveys()
	{
		return $this->hasMany('Survey');
	}

}