<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('uses'=>'HomeController@index', 'before'=>'auth'));
Route::controller('home', 'HomeController');
Route::controller('segment', 'SegmentController');
Route::controller('devices', 'DeviceController');
Route::controller('questions', 'QuestionController');
Route::controller('reportPdf', 'ReportViewController');
//

// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');


//API

Route::group(array('prefix' => 'api/v1'), function(){
	Route::controller('companies', 'ApiCompanyController');
	Route::controller('devices', 'ApiDeviceController');
	Route::controller('places', 'ApiPlaceController');
	Route::controller('segments', 'ApiSegmentController');
	Route::controller('report', 'ReportViewController');	
	//Route::controller('surveys', 'ApiSurveyController');
});